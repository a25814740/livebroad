<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="deleteMember">
            <a href="communityContent.php" class="col-2 pl-0"><span class="icon-back"></span></a>
            <h5 class="col-8 f-24">寶可夢</h5>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="chatListsGroup memberGroup">
            <div class="innerContent">
                <div class="tools text-right">
                    <ul class="tools-wrapper d-inline-flex align-items-center">
                        <li><a href=""><span class="icon-chat f-32"></span></a></li>
                        <li class="ml-3"><a href=""><span class="icon-add-friend2 f-32"></span></a></li>
                        <li class="ml-3"><a href=""><span class="icon-trash f-32"></span></a></li>
                    </ul>
                </div>
                <div class="group">
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/1.jpg)"></div>
                            <span>麻友</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/2.jpg)"></div>
                            <span>咪醬</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/3.jpg)"></div>
                            <span>美金大</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/4.jpg)"></div>
                            <span>小楓</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/5.jpg)"></div>
                            <span>桃子</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/6.jpg)"></div>
                            <span>米莎前輩</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/7.jpg)"></div>
                            <span>仁美</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/8.jpg)"></div>
                            <span>平手友梨奈</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/9.jpg)"></div>
                            <span>栗子</span>
                        </a>
                        <div>
                        </div>
                    </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>