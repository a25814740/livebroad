<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>上傳貼文</h3>
        </header>
        <!-- content -->
        <main class="uploadPostGroup">
            <form action="" class="innerContainer">
                <div class="uploadImgGroup d-flex flex-column justify-content-center align-items-start mb-2">
                    <label for="">上傳圖片：</label>
                    <input type="file" class="w-100">
                    <div class="uploadImg">
                        <div class="imgLabel"></div>
                    </div>
                </div>
                <div class="black d-flex flex-column justify-content-center align-items-start mb-4">
                    <label for="" class="mb-1">輸入說明文字：</label>
                    <textarea name="" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="d-flex justify-content-start align-items-center">
                    <label class="mb-0" for="">設定額度：</label>
                    <div class="select black ml-3">
                        <select name="amount" id="">
                            <option value="10鑽" selected>10鑽</option>
                            <option value="20鑽">20鑽</option>
                            <option value="30鑽">30鑽</option>
                            <option value="40鑽">40鑽</option>
                            <option value="50鑽">50鑽</option>
                            <option value="60鑽">60鑽</option>
                            <option value="70鑽">70鑽</option>
                            <option value="80鑽">80鑽</option>
                            <option value="90鑽">90鑽</option>
                            <option value="100鑽">100鑽</option>
                        </select>
                    </div>
                </div>
                <div class="btnGroup d-flex justify-content-between align-items-center mt-5">
                    <input type="submit" value="確定上傳" class="confirm rounded-pill">
                    <button type="button" class="cancel rounded-pill">取消上傳</button>
                </div>
            </form>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>