<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="sorting">
            <div class="select black">
                <select name="amount" id="selectType">
                    <option value="now">即時性</option>
                    <option value="community">社群</option>
                    <option value="activity">活動</option>
                    <option value="social" selected>社交</option>
                    <option value="game">遊戲</option>
                    <option value="profession">專業</option>
                </select>
            </div>
            <div class="header-right">
                <button type="button" class="no-bg-button sidebar-click-btn">
                    <span class="icon-menu f-38"></span>
                </button>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar mapSidebar">
            <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                <span class="icon-menu f-38"></span>
            </button>
            <div class="accordion" id="accordionExample">
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingOne">
                        <h5 class="mb-0">
                            <button class="mapBtn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                選擇分類
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <ul class="mapCardBody">
                            <li>
                                <a href=>
                                    <div class="icon fri"></div>
                                    <span class="ml-2">好友</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon per"></div>
                                    <span class="ml-2">追蹤</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon hot"></div>
                                    <span class="ml-2">熱門</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon loc"></div>
                                    <span class="ml-2">附近</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingTwo">
                        <h5 class="mb-0">
                            <a href="mapSorting.php" class="mapBtn collapsed d-block">
                                順序排列
                            </a>
                        </h5>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingThree">
                        <h5 class="mb-0">
                            <a href="activityRecord.php" class="mapBtn collapsed d-block">
                                活動紀錄
                            </a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->
        <main class="mapGroup">
            <div id="map" class="map"></div>
            <div class="mapType" data-type="social">
                <button type="button" class="group gameGroup test2">
                    <a href="othersInfo.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">秋元真夏</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="activity">
                <button type="button" class="group gameGroup test2">
                    <a href="activityContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">開學季市集</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="now">
                <button type=" button" class="group gameGroup test2">
                    <a href="nowContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">需要外送飲料</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="profession">
                <button type="button" class="group gameGroup test2">
                    <a href="professionContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">徵求日文家教</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="community">
                <button type=" button" class="group gameGroup test2">
                    <a href="communityIntroduction.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">寶可夢討論群</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="game">
                <button type="button" class="group gameGroup test2">
                    <a href="gameContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">英雄聯盟</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <button class="location" type="button"></button>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        // 初始
        $('#selectType option').each(function(index) {
            if( $(this).prop("selected") == true) {
                var val = $(this).val();
                console.log(val);
                $('[data-type='+val+']').show();
            }
        })
        $('.gameGroup').on('click', function() {
            $(this).find('.person').toggleClass('active');
        })
        $('#selectType').on('change', function() {
            var thisType = $(this).val();
            console.log(thisType);
            switch (thisType) {
                case '社交':
                    $('.mapType').hide();
                    $('[data-type=social]').show();
                    break;
                case '活動':
                    $('.mapType').hide();
                    $('[data-type=activity]').show();
                    break;
                case '即時性':
                    $('.mapType').hide();
                    $('[data-type=now]').show();
                    break;
                case '專業':
                    $('.mapType').hide();
                    $('[data-type=profession]').show();
                    break;
                case '社群':
                    $('.mapType').hide();
                    $('[data-type=community]').show();
                    break;
                case '遊戲':
                    $('.mapType').hide();
                    $('[data-type=game]').show();
                    break;

                default:
                    break;
            }
        })
    });
</script>

</html>