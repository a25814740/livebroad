<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myWallet flex-column">
            <div class="w-100 d-flex justify-content-between align-items-center">
                <a href="myWallet.php" class="col-2"><span class="icon-back"></span></a>
                <h3 class="col-8">支出明細</h3>
                <div class="col-2"></div>
            </div>
            <div class="innerHeader w-100 d-flex justify-content-between align-items-center no-gutters">
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/gift.svg)"></div>
                    <div class="info">
                        <p class="title">我的禮點</p>
                        <p>59347</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/diamond.svg)"></div>
                    <div class="info">
                        <p class="title">我的鑽石</p>
                        <p>3394</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/money.svg)"></div>
                    <div class="info">
                        <p class="title">我的現金</p>
                        <p>70346</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- content -->
        <main class="myWalletGroup">
            <div class="expenditure" id="expenditureGroup">
                <div class="expenditureList expenditureYear">
                    <div class="expenditureYearHeader" id="expenditureYearHeading1">
                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureYearCollapse1" aria-expanded="false">
                            <div class="bg"></div>
                            <div class="col-6">
                                <p class="date">2018</p>
                            </div>
                            <div class="giftImg col-1"></div>
                            <div class="money col-4">17804</div>
                            <div class="allow col-1"></div>
                        </button>
                    </div>
                    <div id="expenditureYearCollapse1" class="collapse" data-parent="#expenditureGroup">
                        <div class="expenditureYearBody">
                            <div class="expenditureMonth" id="expenditureMonthGroup1">
                                <div class="expenditureList expenditureMonth">
                                    <div class="expenditureMonthHeader" id="expenditureMonthHeading1_1">
                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureMonthCollapse1_1" aria-expanded="false">
                                            <div class="bg"></div>
                                            <div class="col-6">
                                                <p class="date">2018/08</p>
                                            </div>
                                            <div class="giftImg col-1"></div>
                                            <div class="money col-4">331</div>
                                            <div class="allow col-1"></div>
                                        </button>
                                    </div>
                                    <div id="expenditureMonthCollapse1_1" class="collapse" data-parent="#expenditureMonthGroup1">
                                        <div class="expenditureMonthBody">
                                            <div class="expenditureDate" id="expenditureDateGroup1_1">
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading1_1_1">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse1_1_1" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2018/09/10</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">163</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse1_1_1" class="collapse" data-parent="#expenditureDateGroup1_1">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading1_1_2">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse1_1_2" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/11</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">197</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse1_1_2" class="collapse" data-parent="#expenditureDateGroup1_1">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="expenditureList expenditureMonth">
                                    <div class="expenditureMonthHeader" id="expenditureMonthHeading1_2">
                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureMonthCollapse1_2" aria-expanded="false">
                                            <div class="bg"></div>
                                            <div class="col-6">
                                                <p class="date">2018/09</p>
                                            </div>
                                            <div class="giftImg col-1"></div>
                                            <div class="money col-4">3970</div>
                                            <div class="allow col-1"></div>
                                        </button>
                                    </div>
                                    <div id="expenditureMonthCollapse1_2" class="collapse" data-parent="#expenditureMonthGroup1">
                                        <div class="expenditureMonthBody">
                                            <div class="expenditureDate" id="expenditureDateGroup1_2">
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading1_2_1">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse1_2_1" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/10</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">163</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse1_2_1" class="collapse" data-parent="#expenditureDateGroup1_2">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading1_2_2">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse1_2_2" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/11</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">197</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse1_2_2" class="collapse" data-parent="#expenditureDateGroup1_2">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="expenditureList expenditureYear">
                    <div class="expenditureYearHeader" id="expenditureYearHeading2">
                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureYearCollapse2" aria-expanded="false">
                            <div class="bg"></div>
                            <div class="col-6">
                                <p class="date">2019</p>
                            </div>
                            <div class="giftImg col-1"></div>
                            <div class="money col-4">15963</div>
                            <div class="allow col-1"></div>
                        </button>
                    </div>
                    <div id="expenditureYearCollapse2" class="collapse" data-parent="#expenditureGroup">
                        <div class="expenditureYearBody">
                            <div class="expenditureMonth" id="expenditureMonthGroup2">
                                <div class="expenditureList expenditureMonth">
                                    <div class="expenditureMonthHeader" id="expenditureMonthHeading2_1">
                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureMonthCollapse2_1" aria-expanded="false">
                                            <div class="bg"></div>
                                            <div class="col-6">
                                                <p class="date">2018/08</p>
                                            </div>
                                            <div class="giftImg col-1"></div>
                                            <div class="money col-4">331</div>
                                            <div class="allow col-1"></div>
                                        </button>
                                    </div>
                                    <div id="expenditureMonthCollapse2_1" class="collapse" data-parent="#expenditureMonthGroup2">
                                        <div class="expenditureMonthBody">
                                            <div class="expenditureDate" id="expenditureDateGroup2_1">
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading2_1_1">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse2_1_1" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/10</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">163</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse2_1_1" class="collapse" data-parent="#expenditureDateGroup2_1">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading2_1_2">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse2_1_2" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/11</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">197</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse2_1_2" class="collapse" data-parent="#expenditureDateGroup2_1">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="expenditureList expenditureMonth">
                                    <div class="expenditureMonthHeader" id="expenditureMonthHeading2_2">
                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureMonthCollapse2_2" aria-expanded="false">
                                            <div class="bg"></div>
                                            <div class="col-6">
                                                <p class="date">2019/09</p>
                                            </div>
                                            <div class="giftImg col-1"></div>
                                            <div class="money col-4">3970</div>
                                            <div class="allow col-1"></div>
                                        </button>
                                    </div>
                                    <div id="expenditureMonthCollapse2_2" class="collapse" data-parent="#expenditureMonthGroup2">
                                        <div class="expenditureMonthBody">
                                            <div class="expenditureDate" id="expenditureDateGroup2_2">
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading2_2_1">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse2_2_1" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/10</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">163</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse2_2_1" class="collapse" data-parent="#expenditureDateGroup2_2">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="expenditureList expenditureDate">
                                                    <div class="expenditureDateHeader" id="expenditureDateHeading2_2_2">
                                                        <button class="row no-gutters expenditureBtn align-items-center" type="button" data-toggle="collapse" data-target="#expenditureDateCollapse2_2_2" aria-expanded="false">
                                                            <div class="bg"></div>
                                                            <div class="col-6">
                                                                <p class="date">2019/09/11</p>
                                                            </div>
                                                            <div class="giftImg col-1"></div>
                                                            <div class="money col-4">197</div>
                                                            <div class="allow col-1"></div>
                                                        </button>
                                                    </div>
                                                    <div id="expenditureDateCollapse2_2_2" class="collapse" data-parent="#expenditureDateGroup2_2">
                                                        <div class="expenditureDateBody">
                                                            <ul class="content">
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/5.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">仁美</span>
                                                                                <span class="account smallText">@97205509</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-11 11:30</p>
                                                                        </div>
                                                                        <span class="type">即時性</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">10</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/3.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">桃子</span>
                                                                                <span class="account smallText">@momogo520</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-08 09:17</p>
                                                                        </div>
                                                                        <span class="type">活動</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">50</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/4.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">咪醬</span>
                                                                                <span class="account smallText">@minamilove</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-07 23:28</p>
                                                                        </div>
                                                                        <span class="type">聊天(贈禮)</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="othersInfo.php" class="no-gutters">
                                                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/myWallet/7.jpg)"></div>
                                                                        <div class="col-5 ml-2">
                                                                            <div>
                                                                                <span class="title">小楓</span>
                                                                                <span class="account smallText">@saidokaede</span>
                                                                            </div>
                                                                            <p class="time smallText">2019-09-05 07:10</p>
                                                                        </div>
                                                                        <span class="type">開啟照片</span>
                                                                        <div class="money col-2">
                                                                            <div class="giftImg"></div>
                                                                            <span class="number">100</span>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        $('.distributionCheck').on('click', function() {
            if ($('.distributionContent').attr('data-check') == 'false') {
                $(this).parents('.distributionContent').attr('data-check', 'true');
                $(this).parents('.distributionList').show();
            } else {
                $(this).parents('.distributionContent').attr('data-check', 'false');
            }
        });
    });
</script>

</html>