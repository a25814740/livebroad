<ul class="nav" id="recommendContect-tab" role="tablist">
    <li class="nav-item mr-2">
        <a class="recommendLink nav-link active" id="myRecommend-tab" data-toggle="pill" href="#myRecommend-content" data-type="myRecommend" role="tab">我的推薦</a>
    </li>
    <li class="nav-item">
        <a class="recommendLink nav-link" id="myOperators-tab" data-toggle="pill" href="#myOperators-content" data-type="myOperators" role="tab">我的經營者</a>
    </li>
    <li>
        <button class="invite" type="button">邀請</button>
    </li>
</ul>
<div class="tab-content mt-3" id="recommendContect-tabContent">
    <div class="tab-pane fade show active review" id="myRecommend-content" role="tabpanel">
        <ul class="flex-ul">
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/1.jpg)"></div>
                    <div class="info">
                        <p class="account">仁美@manaya7320</p>
                        <p class="position smallText">高雄市，小港區</p>
                        <div class="text smallText">
                            <p>2019/08/13</p>
                            <p>19:32</p>
                        </div>
                    </div>
                </div>
                <button class="delBtn rounded-pill">刪除</button>
            </li>
        </ul>
        <div class="intro d-flex justify-content-between align-items-end">
            <p>推薦我</p>
            <span class="f-12">是否活躍</span>
        </div>
        <ul class="recommendMe flex-ul">
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/3.jpg)"></div>
                    <div class="info">
                        <p class="account">麻友@gcian_48</p>
                        <p class="position smallText">台南市，中西區</p>
                        <div class="text smallText">
                            <p>2019/09/03</p>
                            <p>07:11</p>
                        </div>
                    </div>
                </div>
                <button class="yes"></button>
            </li>
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/4.jpg)"></div>
                    <div class="info">
                        <p class="account">咪醬@minami_48</p>
                        <p class="position smallText">桃園市，大園區</p>
                        <div class="text smallText">
                            <p>2019/09/15</p>
                            <p>23:26</p>
                        </div>
                    </div>
                </div>
                <button class="no"></button>
            </li>
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/5.jpg)"></div>
                    <div class="info">
                        <p class="account">美月@nogizaka_46</p>
                        <p class="position smallText">高雄市，前鎮區</p>
                        <div class="text smallText">
                            <p>2019/10/25</p>
                            <p>12:40</p>
                        </div>
                    </div>
                </div>
                <button class="yes"></button>
            </li>
        </ul>
    </div>
    <div class="tab-pane fade" id="myOperators-content" role="tabpanel">
        <ul class="nav justify-content-between align-items-center" id="inner-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="structure-tab" data-toggle="pill" href="#structure-content" role="tab">經營架構</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="list-tab" data-toggle="pill" href="#list-content" role="tab">審核名單</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="distribution-tab" data-toggle="pill" href="#distribution-content" role="tab">推薦分配</a>
            </li>
        </ul>
        <div class="tab-content operators" id="inner-tabContent">
            <div class="tab-pane fade show active structureContent" id="structure-content" role="tabpanel">
                <ul class="flex-ul">
                    <li class="pl-2">
                        <span class="f-12">我的領導</span>
                    </li>
                    <li class="pr-4">
                        <div class="d-flex justify-content-start align-items-center">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/1.jpg)"></div>
                            <div class="info">
                                <p class="account">仁美@manaya7320</p>
                                <p class="position smallText">高雄市，小港區</p>
                                <div class="text smallText">
                                    <p>2019/08/13</p>
                                    <p>19:32</p>
                                </div>
                            </div>
                        </div>
                        <button class="changeBtn rounded-pill">更換</button>
                    </li>
                </ul>
                <div class="intro d-flex justify-content-between align-items-center f-12 no-gutters">
                    <span class="flex-60 pl-2">第一經營者</span>
                    <span class="flex-20">消費額度</span>
                    <span class="flex-20">領導獎金</span>
                </div>
                <ul class="detail detailExpansion" id="detailExpansion">
                    <li>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="flex-60 d-flex justify-content-start align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/3.jpg)"></div>
                                <div class="info">
                                    <p class="account">麻友@gcian_48</p>
                                    <p class="position smallText">台南市，中西區</p>
                                    <div class="text smallText">
                                        <p>2019/09/03</p>
                                        <p>07:11</p>
                                    </div>
                                </div>
                            </div>
                            <span class="flex-20">3199</span>
                            <span class="flex-20">4718</span>
                        </div>
                        <div class="type">
                            <button class="expansion" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false">
                                經營者
                            </button>
                            <p>推薦者人數:39</p>
                        </div>
                        <div id="collapse1" class="collapse" data-parent="#detailExpansion">
                            <ul class="detailBody">
                                <li class="title">第一層</li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 leader">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/6.jpg)"></div>
                                            <div class="info">
                                                <p class="account">美月@nogizaka_46</p>
                                                <p class="position smallText">台南市，安平區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/15</p>
                                                    <p>23:26</p>
                                                </div>
                                            </div>
                                            <div class="number">推薦者人數:11</div>
                                        </div>
                                        <span class="flex-20">279</span>
                                        <span class="flex-20">771</span>
                                    </div>
                                </li>
                                <li class="mid f-12">月消費額度</li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 subordinate">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/9.jpg)"></div>
                                            <div class="info">
                                                <p class="account">咪醬@minami_48</p>
                                                <p class="position smallText">嘉義市，集集區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/12</p>
                                                    <p>20:17</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="amount">
                                            <div class="money no-gutters text-center mb-2">
                                                <div class="w-50 smallNumber">31</div>
                                            </div>
                                            <div class="affiliation">
                                                <p>隸屬:</p>
                                                <p>七瀨@nanase25</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 subordinate">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/8.jpg)"></div>
                                            <div class="info">
                                                <p class="account">小楓@kaeten</p>
                                                <p class="position smallText">高雄市，苓雅區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/11</p>
                                                    <p>08:25</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="amount">
                                            <div class="money no-gutters text-center mb-2">
                                                <div class="w-50 smallNumber">76</div>
                                            </div>
                                            <div class="affiliation">
                                                <p>隸屬:</p>
                                                <p>七瀨@nanase25</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="title">第二層</li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 leader">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/7.jpg)"></div>
                                            <div class="info">
                                                <p class="account">桃子@momoko</p>
                                                <p class="position smallText">台南市，中西區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/14</p>
                                                    <p>11:26</p>
                                                </div>
                                            </div>
                                            <div class="number">推薦者人數:23</div>
                                        </div>
                                        <span class="flex-20">137</span>
                                        <span class="flex-20">301</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="flex-60 d-flex justify-content-start align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/4.jpg)"></div>
                                <div class="info">
                                    <p class="account">琴子@kotoko</p>
                                    <p class="position smallText">高雄市，阿蓮區</p>
                                    <div class="text smallText">
                                        <p>2019/09/15</p>
                                        <p>23:17</p>
                                    </div>
                                </div>
                            </div>
                            <span class="flex-20">2180</span>
                            <span class="flex-20">3927</span>
                        </div>
                        <div class="type">
                            <button class="expansion" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false">
                                經營者
                            </button>
                            <p>推薦者人數:39</p>
                        </div>
                        <div id="collapse2" class="collapse" data-parent="#detailExpansion">
                            <ul class="detailBody">
                                <li class="title">第一層</li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 leader">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/6.jpg)"></div>
                                            <div class="info">
                                                <p class="account">美月@nogizaka_46</p>
                                                <p class="position smallText">台南市，安平區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/15</p>
                                                    <p>23:26</p>
                                                </div>
                                            </div>
                                            <div class="number">推薦者人數:11</div>
                                        </div>
                                        <span class="flex-20">279</span>
                                        <span class="flex-20">771</span>
                                    </div>
                                </li>
                                <li class="mid">
                                    <span class="flex-60"></span>
                                    <span class="flex-20 f-12">月消費額度</span>
                                    <span class="flex-20"></span>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 subordinate">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/9.jpg)"></div>
                                            <div class="info">
                                                <p class="account">咪醬@minami_48</p>
                                                <p class="position smallText">嘉義市，集集區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/12</p>
                                                    <p>20:17</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="amount">
                                            <div class="money no-gutters text-center mb-2">
                                                <div class="w-50 smallNumber">31</div>
                                            </div>
                                            <div class="affiliation">
                                                <p>隸屬:</p>
                                                <p>七瀨@nanase25</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 subordinate">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/8.jpg)"></div>
                                            <div class="info">
                                                <p class="account">小楓@kaeten</p>
                                                <p class="position smallText">高雄市，苓雅區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/11</p>
                                                    <p>08:25</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="amount">
                                            <div class="money no-gutters text-center mb-2">
                                                <div class="w-50 smallNumber">76</div>
                                            </div>
                                            <div class="affiliation">
                                                <p>隸屬:</p>
                                                <p>七瀨@nanase25</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="title">第二層</li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center w-100 leader">
                                        <div class="flex-60 d-flex justify-content-start align-items-start">
                                            <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/7.jpg)"></div>
                                            <div class="info">
                                                <p class="account">桃子@momoko</p>
                                                <p class="position smallText">台南市，中西區</p>
                                                <div class="text smallText">
                                                    <p>2019/09/14</p>
                                                    <p>11:26</p>
                                                </div>
                                            </div>
                                            <div class="number">推薦者人數:23</div>
                                        </div>
                                        <span class="flex-20">137</span>
                                        <span class="flex-20">301</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="tab-pane fade listContent" id="list-content" role="tabpanel" data-select="false">
                <ul>
                    <li class="intro d-flex justify-content-between align-items-center f-12 no-gutters">
                        <span class="flex-60"></span>
                        <span class="flex-20 f-14">受推薦人數</span>
                        <span class="flex-20"></span>
                    </li>
                    <li class="reviewList">
                        <div class="d-flex justify-content-between align-items-center w-100 list">
                            <div class="flex-60 d-flex justify-content-start align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/8.jpg)"></div>
                                <div class="info">
                                    <p class="account">娜娜敏@mamami</p>
                                    <p class="position smallText">台南市，東區</p>
                                    <div class="text smallText">
                                        <p>2019/09/03</p>
                                        <p>07:11</p>
                                    </div>
                                </div>
                            </div>
                            <div class="selectSection">
                                <div>
                                    <span class="number">220</span>
                                    <div class="iconGroup">
                                        <div class="icon sel changeSelect"></div>
                                        <div class="icon del ml-3"></div>
                                    </div>
                                </div>
                                <ul class="starGroup">
                                    <li class="yes"></li>
                                    <li class="yes"></li>
                                    <li class="yes"></li>
                                </ul>
                            </div>
                        </div>
                        <div class="type">
                            <div class="month">
                                <span class="small">月消費額</span>
                                <span>216</span>
                            </div>
                            <div class="total">
                                <span class="small">總消費額</span>
                                <span>3461</span>
                            </div>
                        </div>
                        <div class="change">
                            <!-- <button class="active" type="button">調換</button>
                            <button class="ml-3" type="button">分配</button> -->
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radios" id="radios1" value="option1" checked>
                                <label class="form-check-label" for="radios1">調換</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radios" id="radios2" value="option2">
                                <label class="form-check-label" for="radios2">分配</label>
                            </div>
                        </div>
                    </li>
                    <li class="reviewList">
                        <div class="d-flex justify-content-between align-items-center w-100 list">
                            <div class="flex-60 d-flex justify-content-start align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/4.jpg)"></div>
                                <div class="info">
                                    <p class="account">琴子@ktk_46</p>
                                    <p class="position smallText">高雄市，苓雅區</p>
                                    <div class="text smallText">
                                        <p>2019/09/06</p>
                                        <p>15:34</p>
                                    </div>
                                </div>
                            </div>
                            <div class="selectSection">
                                <div>
                                    <span class="number">73</span>
                                    <div class="iconGroup">
                                        <div class="icon"></div>
                                        <div class="icon del ml-3"></div>
                                    </div>
                                </div>
                                <ul class="starGroup">
                                    <li class="yes"></li>
                                    <li class="redStar"></li>
                                    <li class="whiteStar"></li>
                                </ul>
                            </div>
                        </div>
                        <div class="type">
                            <div class="month">
                                <span class="small">月消費額</span>
                                <span>216</span>
                            </div>
                            <div class="total">
                                <span class="small">總消費額</span>
                                <span>2278</span>
                            </div>
                        </div>
                    </li>
                    <li class="reviewList">
                        <div class="d-flex justify-content-between align-items-center w-100 list">
                            <div class="flex-60 d-flex justify-content-start align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/7.jpg)"></div>
                                <div class="info">
                                    <p class="account">里奈@ikumarina</p>
                                    <p class="position smallText">台南市，歸仁區</p>
                                    <div class="text smallText">
                                        <p>2019/09/06</p>
                                        <p>15:34</p>
                                    </div>
                                </div>
                            </div>
                            <div class="selectSection">
                                <div>
                                    <span class="number">46</span>
                                    <div class="iconGroup">
                                        <div class="icon"></div>
                                        <div class="icon del ml-3"></div>
                                    </div>
                                </div>
                                <ul class="starGroup">
                                    <li class="redStar"></li>
                                    <li class="redStar"></li>
                                    <li class="yes"></li>
                                </ul>
                            </div>
                        </div>
                        <div class="type">
                            <div class="month">
                                <span class="small">月消費額</span>
                                <span>391</span>
                            </div>
                            <div class="total">
                                <span class="small">總消費額</span>
                                <span>3461</span>
                            </div>
                        </div>
                    </li>
                    <li class="innerDel"></li>
                    <form action="" class="innerInput">
                        <label for="">請輸入ID:</label>
                        <div>
                            <input type="text">
                            <input class="rounded-pill" type="submit" value="確定">
                        </div>
                    </form>
                </ul>
            </div>
            <div class="tab-pane fade distributionContent" id="distribution-content" role="tabpanel" data-check="false">
                <div class="intro d-flex justify-content-end align-items-center f-12 no-gutters">
                    <span class="flex-60"></span>
                    <div class="money">
                        <div class="w-50 smallText">月消費額</div>
                        <div class="w-50 smallText">總消費額</div>
                    </div>
                </div>
                <form action="">
                    <ul>
                        <li class="distributionList">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-60 leftBox">
                                    <input class="distributionCheck" type="checkbox">
                                    <div class="d-flex justify-content-start align-items-center">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/2.jpg)"></div>
                                        <div class="info">
                                            <p class="account">ten醬@ten_934571</p>
                                            <p class="position smallText">嘉義市，集集區</p>
                                            <div class="text smallText">
                                                <p>2019/09/15</p>
                                                <p>23:17</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="money">
                                    <div class="w-50 smallNumber">391</div>
                                    <div class="w-50 smallNumber">3461</div>
                                </div>
                            </div>
                        </li>
                        <li class="distributionList">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-60 leftBox">
                                    <div class="d-flex justify-content-center align-items-center">
                                        <input class="distributionCheck" type="checkbox">
                                    </div>
                                    <div class="d-flex justify-content-start align-items-center">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/3.jpg)"></div>
                                        <div class="info">
                                            <p class="account">米莎前輩@misamisa</p>
                                            <p class="position smallText">台南市，安平區</p>
                                            <div class="text smallText">
                                                <p>2019/09/02</p>
                                                <p>06:49</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="money">
                                    <div class="w-50 smallNumber">245</div>
                                    <div class="w-50 smallNumber">1487</div>
                                </div>
                            </div>
                        </li>
                        <li class="distributionList">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-60 leftBox">
                                    <div class="d-flex justify-content-center align-items-center">
                                        <input class="distributionCheck" type="checkbox">
                                    </div>
                                    <div class="d-flex justify-content-start align-items-center">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/4.jpg)"></div>
                                        <div class="info">
                                            <p class="account">桃子@momoko</p>
                                            <p class="position smallText">高雄市，小港區</p>
                                            <div class="text smallText">
                                                <p>2019/08/11</p>
                                                <p>07:34</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="money">
                                    <div class="w-50 smallNumber">226</div>
                                    <div class="w-50 smallNumber">2679</div>
                                </div>
                            </div>
                        </li>
                        <li class="distributionList">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-60 leftBox">
                                    <div class="d-flex justify-content-center align-items-center">
                                        <input class="distributionCheck" type="checkbox">
                                    </div>
                                    <div class="d-flex justify-content-start align-items-center">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/5.jpg)"></div>
                                        <div class="info">
                                            <p class="account">平手友梨奈@yurina_46</p>
                                            <p class="position smallText">屏東市，潮州區</p>
                                            <div class="text smallText">
                                                <p>2019/08/30</p>
                                                <p>11:48</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="money">
                                    <div class="w-50 smallNumber">134</div>
                                    <div class="w-50 smallNumber">1148</div>
                                </div>
                            </div>
                        </li>
                        <li class="distributionList">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="flex-60 leftBox">
                                    <div class="d-flex justify-content-center align-items-center">
                                        <input class="distributionCheck" type="checkbox">
                                    </div>
                                    <div class="d-flex justify-content-start align-items-center">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/6.jpg)"></div>
                                        <div class="info">
                                            <p class="account">栗子@suzudo7321</p>
                                            <p class="position smallText">台南市，中西區</p>
                                            <div class="text smallText">
                                                <p>2019/09/03</p>
                                                <p>07:11</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="money">
                                    <div class="w-50 smallNumber">391</div>
                                    <div class="w-50 smallNumber">3461</div>
                                </div>
                            </div>
                        </li>
                        <li class="distributionInput">
                            <form action="">
                                <label for="">請輸入ID:</label>
                                <div>
                                    <input type="text">
                                    <input class="rounded-pill" type="submit" value="確定">
                                </div>
                            </form>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>