<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="media row">
            <div class="col-3"></div>
            <h3 class="col-6">媒體影音</h3>
            <div class="col-3 text-right">
                <button type="button" class="no-bg-button sidebar-click-btn">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar">
            <div class="sidebar-header">
                <button type="button" class="close-button icons sidebar-click-btn">
                    <span class="icon-menu"></span>
                </button>
            </div>
            <div class="sidebar-content">
                <ul>
                    <li>
                        <a href="mediaIndex.php" class="d-flex align-items-center active">
                            <span class="icon-video1 f-18 mr-2"></span>
                            <span>所有影片</span>
                        </a>
                    </li>
                    <li>
                        <a href="watchRecord.php" class="d-flex align-items-center">
                            <span class="icon-history f-18 mr-2"></span>
                            <span>觀看記錄</span>
                        </a>
                    </li>
                    <li>
                        <a href="uploadLink.php" class="d-flex align-items-center">
                            <span class="icon-upload f-18 mr-2"></span>
                            <span>上傳連結</span>
                        </a>
                    </li>
                    <li>
                        <a href="myCollection.php" class="d-flex align-items-center">
                            <span class="icon-bookMark f-18 mr-2"></span>
                            <span>我的收藏</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- content -->
        <main class="mediaGroup">
            <div class="innerContainer">
                <div class="innerHeader">
                    <form action="">
                        <div class="custom-search">
                            <input type="text" class="custom-search-input" required>
                            <input type="submit" value="">
                            <span class="icon-search"></span>
                        </div>
                    </form>
                </div>
                <ul class="videoGroup">
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">4:09</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">Rev.from DVL「LOVE-arigatou-」橋本環奈</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>46萬</span></div>
                                <span class="times">1天前</span>
                            </div>
                        </div>
                    </li>
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">1:55</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">亞馬遜雨林正以前所未見的規模猛烈燃燒《國家地理》雜誌</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>11萬</span></div>
                                <span class="times">3天前</span>
                            </div>
                        </div>
                    </li>
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">4:09</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">Rev.from DVL「LOVE-arigatou-」橋本環奈</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>46萬</span></div>
                                <span class="times">1天前</span>
                            </div>
                        </div>
                    </li>
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">1:55</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">亞馬遜雨林正以前所未見的規模猛烈燃燒《國家地理》雜誌</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>11萬</span></div>
                                <span class="times">3天前</span>
                            </div>
                        </div>
                    </li>
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">4:09</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">Rev.from DVL「LOVE-arigatou-」橋本環奈</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>46萬</span></div>
                                <span class="times">1天前</span>
                            </div>
                        </div>
                    </li>
                    <li class="item row no-gutters justify-content-center align-items-center">
                        <div class="col-6 video" style="background-image:url(styles/images/myCollection/10.jpg)">
                            <span class="video-time">1:55</span>
                        </div>
                        <div class="col-6 pl-1">
                            <span class="title">亞馬遜雨林正以前所未見的規模猛烈燃燒《國家地理》雜誌</span>
                            <div class="info d-flex justify-content-start align-items-center">
                                <div class="watchs">觀看次數:<span>11萬</span></div>
                                <span class="times">3天前</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>