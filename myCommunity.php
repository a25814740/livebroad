<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myCommunity">
            <a href="community.php" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">我的社群</h3>
            <a href="createCommunity.php" class="add col-2 icon"></a>
        </header>
        <!-- content -->
        <main class="myCommunityGroup">
            <div class="innerContent h-100">
                <div class="navGroup d-flex justify-content-between align-items-center">
                    <ul class="nav justify-content-start align-items-center" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a href="#pills-content0" class="nav-link active" id="pills-tab0" data-toggle="pill">全部</a>
                        </li>
                        <li class="nav-item">
                            <a href="#pills-content1" class="nav-link" id="pills-tab1" data-toggle="pill">動漫</a>
                        </li>
                        <li class="nav-item">
                            <a href="#pills-content2" class="nav-link" id="pills-tab2" data-toggle="pill">遊戲</a>
                        </li>
                    </ul>
                    <a href="javascript:;" id="navAdd" class="addMenu"></a>
                    <div id="editBtn" class="editBtn">
                        <a href="communityClassification.php" id="pen" class="pen"></a>
                        <a href="javascript:;" id="del" data-method="del" data-type="auto" class="layui-btn layui-btn-normal del"></a>
                    </div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-content0" role="tabpanel">
                        <ul class="group"></ul>
                    </div>
                    <div class="tab-pane fade" id="pills-content1" role="tabpanel" data-id="item">
                        <ul class="group">
                            <li>
                                <a href="communityContent.php" class="news">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">LOL討論群(359)</span>
                                    <button type="button" class="lock no"></button>
                                </a>
                            </li>
                            <li>
                                <a href="communityContent.php" class="news">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">芭樂咖蹦脆(6)</span>
                                    <button type="button" class="lock yes"></button>
                                </a>
                            </li>
                            <li>
                                <a href="communityContent.php">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">我們這一班315(37)</span>
                                    <button type="button" class="lock yes"></button>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="pills-content2" role="tabpanel" data-id="item">
                        <ul class="group">
                            <li>
                                <a href="communityContent.php">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">坂道遊戲群(190)</span>
                                    <button type="button" class="lock no"></button>
                                </a>
                            </li>
                            <li>
                                <a href="communityContent.php">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">懷念京阿尼(542)</span>
                                    <button type="button" class="lock no"></button>
                                </a>
                            </li>
                            <li>
                                <a href="communityContent.php" class="news">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">DEEMO(39)</span>
                                    <button type="button" class="lock no"></button>
                                </a>
                            </li>
                            <li>
                                <a href="communityContent.php" class="news">
                                    <span class="new"></span>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                                    <span class="title">耍廢交流(87)</span>
                                    <button type="button" class="lock no"></button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <div id="copyUse" class="d-none">
            <li class="nav-item">
                <a href="#pills-content" class="nav-link" data-toggle="pill">名稱</a>
            </li>
            <div class="tab-pane fade" role="tabpanel" data-id="item">
                <ul class="group"></ul>
            </div>
        </div>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        allItem();
        // 新增項目按鈕事件
        $('#navAdd').on('click', function() {
            var number = $('#pills-tab .nav-item').length;
            $('#pills-tab').append($('#copyUse .nav-item').clone());
            $('#pills-tabContent').append($('#copyUse .tab-pane').clone());
            $('#pills-tab .nav-item:last').find('a').attr({
                'href': '#pills-content' + number,
                'id': 'pills-tab' + number
            });
            $('#pills-tabContent .tab-pane:last').attr('id', 'pills-content' + number);
        });
        // 判別是否為ALL項目
        $('.nav-link').on('click', function() {
            var thisId = $(this).attr('id');
            if (thisId == 'pills-tab0') {
                $('#navAdd').show();
                $('#editBtn').hide();
            } else {
                $('#navAdd').hide();
                $('#editBtn').show();
            }
        })
    });

    function allItem() {
        var html = '',
            group = $('#pills-tabContent [data-id=item] .group');
        // 抓取每個項目內容
        group.each(function() {
            html += $(this).html();
        })
        // 將資料放入ALL項目
        $('#pills-content0 .group').html(html);
    }
</script>

</html>