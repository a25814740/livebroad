// 訊息 解鎖 alert
function unLockPic() {
    alert('確定要花費80鑽石來解鎖此圖片嗎?')
    window.location.href = "#"
}
// 訊息 封鎖 alert
function leaveChat() {
    alert('確定要離開群組嗎?')
    window.location.href = "#"
}

function star() {
    $('.star').each(function(){
        $(this).find('.img').click(function() {
            var total = $('.img').length,
                no = $(this).index()+1;
                console.log(total);
                console.log(no);
            $(this).parents('.star').find('.img').removeClass('yes');
            for( var i = 0; i < no; i++) {
                $(this).parents('.star').find('.img').eq(i).removeClass('no');
                $(this).parents('.star').find('.img').eq(i).addClass('yes');
            }
        });
    })
}

$('.gift').on('click', function() {
    $('.gift').removeClass('active');
    $(this).addClass('active');
})

$(function() {

    // dotdotdot 這邊可以複製起來，放到personInfo.php跟othersInfo.php最下方
    // 然後這段直接註解掉
    // read more
	$(".dotdotdot").dotdotdot({
		height: 60,
		fallbackToLetter: true,
        watch: true,
        ellipsis: "\u2026 Read More",
        truncate: "word",
    });
    $(".dotdotdot-line-1").dotdotdot({
		height: 32
    });
    $(".dotdotdot-line-3").dotdotdot({
		height: 60,
		fallbackToLetter: true,
        watch: true,
        ellipsis: "\u2026 Read More",
        truncate: "word"
    });
    $(".dotdotdot-line-4").dotdotdot({
		height: 80,
		fallbackToLetter: true,
        watch: true,
        ellipsis: "\u2026 Read More",
        truncate: "word"
    });

    // 評價
    star();

    // 側邊欄
    sideberClick();

    // icon 點擊新增Class
    iconActive();

    // Header Tools 開闔
    $('.chat-header .menu .menu-ico').click(function(){
        $('.chat-header .menu ul.list').slideToggle('fast');
    });
    // 點空白處 Header Tools 關閉
    $(document).click(function(){
        $(".chat-header .menu ul.list").slideUp('fast');
    });
    // 防止跳轉
    $(".chat-header .menu ul.list, .chat-inp-tools-wrapper, .chat-header .menu .menu-ico").click(function(e){
        e.stopPropagation();
    });
    // 點空白處 Header Tools 關閉
    $(document).click(function(){
        $(".chat-inp-tools-wrapper").removeClass('open');
    });
    // 點擊開啟表情選單
    $('.chat-inp-tools-btn').click(function(){
        $('.chat-inp-tools-wrapper').toggleClass('open');
    });
    // 點擊開啟 表情選單
    $('.chat-inp .emoji').click(function(){
        $('.emoji-dashboard').slideToggle('fast');
        $(".chat-inp-tools-wrapper").removeClass('open');
    });
    $('.chat-inp .chat-tool-presents').click(function(){
        // $('.presents-dashboard').slideToggle('fast');
        $('.presents-dashboard').addClass('open')
        $(".chat-inp-tools-wrapper").removeClass('open');
    });
    $('.presents-dashboard.position-fixed header a, .presents-dashboard.position-fixed .giftBtn').click(function() {
        $('.presents-dashboard').removeClass('open');
    })
    // 點空白處 表情選單關閉
    $(document).click(function(){
        $(".tools-dashboard").slideUp('fast');
    });
    // 防止跳轉
    $(".chat-header .menu ul.list, .chat-inp-tools, .chat-inp .emoji").click(function(e){
        e.stopPropagation();
    });
    // 點擊 表情<i> 後插入對話框 並關閉表情選單
    $('.tools-dashboard li .em').click(function(){
        // var emo = $(this).css('background-image').split('"')[1];
        // $('.chat-inp .input').find('div').remove();
        // $('.chat-inp .input').append('<img data-emoji-type="font" src="'+emo+'">');
        $(".tools-dashboard").slideUp('fast');
    });
    // 點擊 表情<img> 後插入對話框 並關閉表情選單
    $('.tools-dashboard li img').click(function(){
        // $('.chat-inp .input').find('div').remove();
        // $('.chat-inp .input').append($(this));
        $(".tools-dashboard").slideUp('fast');
    });
    // $('.chat-inp .opts .send').click(function(){
    //     var val = $('.chat-inp .input').html();
    //     if (val.length > 0){
    //     $('.chat-body .chats-text-cont').append('<p class="chat-text text-right"><span class="text-buble">'+val+'</span><span class="time">13:45</span></p>')
    //     }
    //     $('.chat-inp .input').html('');
    //     $('.chats-text-cont div').remove();
    // });
    // $('input,.input').each(function(){
    //     tmpval = $(this).text().length;
    //     if(tmpval != '') {
    //         $(this).prev().addClass('trans');
    //         $(this).parent().addClass('lined');
    //     }
    // });
    // $('input, .input').focus(function() {
    //     $(this).prev().addClass('trans');
    //     $(this).parent().addClass('lined');
    //     $(document).keypress(function(e) {
    //         if(e.which == 13) {
    //             $('.chat-inp .opts .send').click();
    //         }
    //     });
    // }).blur(function() {
    //     if ($(this).text().length == ''){
    //         $(this).prev().removeClass('trans');
    //         $(this).parent().removeClass('lined');
    //     }
    // });
    
  
})

// layui.use('laydate', function(){
//     var laydate = layui.laydate;
//     //传入Date对象给初始值
//     laydate.render({ 
//         elem: '[data-input-type="date"]',
//         format: 'yyyy/MM/dd',
//         showBottom: false
//     });
// });
// layui.use('laydate', function(){
//     var laydate = layui.laydate;
//     //传入Date对象给初始值
//     laydate.render({ 
//         elem: '[data-input-type="time"]',
//         type: 'time',
//         format: 'HH:mm:ss'
//     });
// });

// layui.use('laydate', function(){
//     var laydate = layui.laydate;
//     //传入Date对象给初始值
//     laydate.render({ 
//         elem: '#form-input5',
//         format: 'yyyy/MM/dd',
//         showBottom: false
//     });

//     laydate.render({ 
//         elem: '#date',
//         format: 'yyyy/MM/dd',
//         showBottom: false
//     });

//     laydate.render({ 
//         elem: '#time',
//         type: 'time',
//         format: 'HH:mm:ss'
//     });
// });

// $('#form-input5').datepicker({
//     dateFormat: 'yy/mm/dd'
// });

function sideberClick() {
    $('.sidebar-click-btn').each(function() {
        $(this).on('click', function() {
            console.log('click');
            $('body').toggleClass("sidebar-open");
        })
    })
}


var active = {
    offset: function(othis){
        var type = othis.attr('data-type')
            ,label = othis.siblings('label').find('span').text()
            ,text = '<input type="text" placeholder="'+'新增'+label+'">';
        layer.open({
            type: 1
            ,title: false
            ,offset: type
            ,id: 'layerBtn'+type
            ,content: '<div style="padding: 30px 20px 10px 20px;">'+ text +'</div>'
            ,btn: '新增'
            ,btnAlign: 'c' //按钮居中
            ,shade: 0 //不显示遮罩
            ,yes: function(){
                var $org = othis.siblings('div').find('input'),
                    val_org = $org.val(),
                    val_new = $('.layui-layer input').val();
                $org.val(val_org + ' ' +val_new);
                layer.closeAll();
            }
        });
        
        layer.config({
            extend: '../plugins/layui/skin/default/style.css',
            skin: 'layer-ext-default'
        });
    },
    
    del: function(othis){
        var type = othis.data('type')
            ,label = othis.siblings('label').find('span').text()
            ,text = '確定要刪除此分類嗎?';

        var thisTab = $('.nav').find('.active'),
            thisContent = $('#pills-tabContent').find('.active');
        layer
            .open({
                type: 1
                ,title: false
                ,offset: type
                ,id: 'layerDelBtn'+type
                ,content: '<div style="padding: 20px;border-bottom: solid 1px #ccc;">'+ text +'</div>'
                ,btn: '確定'
                ,btnAlign: 'c' //按钮居中
                ,shade: .4 //不显示遮罩
                ,shadeClose: true
                ,closeBtn: false
                ,yes: function() {
                    layer.closeAll();
                    thisTab.remove();
                    thisContent.remove();
                    $('#pills-tab0').addClass('active');
                    $('#pills-content0').addClass('active show');
                    $('#navAdd').show();
                    $('#editBtn').hide();
                    allItem();
                }
                
            }) 
        layer
            .config({
                extend: '../plugins/layui/skin/del/style.css',
                skin: 'layer-ext-del'
            });
    },
    
    leave: function(othis){
        var type = othis.data('type')
            ,label = othis.siblings('label').find('span').text()
            ,text = '確定要離開社群嗎?';

        var thisTab = $('.nav').find('.active'),
            thisContent = $('#pills-tabContent').find('.active');
        layer
            .open({
                type: 1
                ,title: false
                ,offset: type
                ,id: 'layerDelBtn'+type
                ,content: '<div style="padding: 20px;border-bottom: solid 1px #ccc;">'+ text +'</div>'
                ,btn: '確定'
                ,btnAlign: 'c' //按钮居中
                ,shade: .4 //不显示遮罩
                ,shadeClose: true
                ,closeBtn: false
                ,yes: function() {
                    layer.closeAll();
                }
                
            }) 
        layer
            .config({
                extend: '../plugins/layui/skin/del/style.css',
                skin: 'layer-ext-del'
            });
    },
    
    storeValue: function(othis){
        var type = othis.data('type')
            ,label = othis.siblings('label').find('span').text()
            ,text = '確定要儲值80鑽嗎?';

        layer
            .open({
                type: 1
                ,title: false
                ,offset: type
                ,id: 'layerDelBtn'+type
                ,content: '<div style="padding: 20px;border-bottom: solid 1px #ccc;">'+ text +'</div>'
                ,btn: ['確定', '取消']
                ,btnAlign: 'c' //按钮居中
                ,shade: .4 //不显示遮罩
                ,shadeClose: true
                ,closeBtn: false
                ,yes: function() {
                    layer.closeAll();
                }
                
            }) 
        layer
            .config({
                extend: '../plugins/layui/skin/del/style.css',
                skin: 'layer-ext-del'
            });
    },
    
    unlock: function(othis){ // button的data-method名稱
        var type = othis.data('type')
            ,label = othis.siblings('label').find('span').text()
            ,text = '確定要花費80鑽石來解鎖此圖片嗎?';

        var thisTab = $('.nav').find('.active'),
            thisContent = $('#pills-tabContent').find('.active');
            
        layer
            .open({
                type: 1
                ,title: false
                ,offset: type
                ,id: 'layerUnlockBtn'+type // 按鈕的id名稱
                ,content: '<div style="padding: 20px;border-bottom: solid 1px #ccc;">'+ text +'</div>' // 彈窗內容
                ,btn: '確定' // 如果要加其他按鈕使用陣列寫法 ['確定', '按鈕2']
                ,btnAlign: 'c' //按钮居中
                ,shade: .4 //不显示遮罩
                ,shadeClose: true // 點遮罩是否關閉彈窗
                ,closeBtn: false // 是否要右上角的叉叉關閉按鈕
                ,yes: function(index, layero) { // 按下確定之後 do something
                    console.log(layero);
                    othis.removeClass('blur-item layui-btn').attr('data-method', '');
                    othis.find('.blur').removeClass('blur');
                    othis.parents('.blur-wrapper').find('.blur.image').remove();
                    othis.parents('.blur-wrapper').find('.play-btn').remove();
                    othis.parents('.blur-wrapper').find('.blur').removeClass('blur');
                    othis.parents('.blur-wrapper').removeClass('blur-wrapper');
                    othis.find('.text').remove();

                    // 新增另開彈窗
                    layer.close(layer.index);
                
                    var money = 0;
                    if(  money < 1 ) {
                        layer
                            .open({
                                type: 1
                                ,title: false
                                ,offset: type
                                ,content: '<div style="padding: 20px;border-bottom: solid 1px #ccc;">鑽石不足</div>' // 彈窗內容
                                ,btn: '前往儲值' // 如果要加其他按鈕使用陣列寫法 ['確定', '按鈕2']
                                ,btnAlign: 'c' //按钮居中
                                ,shade: .4 //不显示遮罩
                                ,shadeClose: true // 點遮罩是否關閉彈窗
                                ,closeBtn: false // 是否要右上角的叉叉關閉按鈕
                                ,yes: function(index, layero) { // 按下確定之後 do something
                                    window.location.href="stored.php"
                                }
                            }) 
                    }
                }
            }) 
        layer
            .config({
                extend: '../plugins/layui/skin/del/style.css',
                skin: 'layer-ext-del'
            });
    }
};

$('.layui-btn').each(function() {
    var othis = $(this);
    othis.on('click', function(){
        var method = othis.attr('data-method');
        active[method] ? active[method].call(this, othis) : '';
        console.log('object');
    })
});

$(window).on('load', function() {
    $('.ddd-truncated').each(function() {
        $(this).click(function() {
            layer
                .open({
                    type: 1 
                    ,title: false
                    ,closeBtn: 0
                    ,shade: 0.8
                    ,shadeClose: true
                    ,anim: 5
                    ,content: '<div class="p-4">Graphic designer / Photoshop / Illustrator / 2D generalist / EN/ESP / contact: camilacarbia@gmail.com</div>'
                });
            layer
                .config({
                    extend: '../plugins/layui/skin/person/style.css',
                    skin: 'layer-ext-del'
                });
        })
    })
})

// icon 點擊新增Class
function iconActive() {
    $('.icon-active-area [class*="icon"]').click(function() {
        $(this).toggleClass('active');
    })
}