<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="communityContent">
            <a href="personInfo.php" class="col-2"><span class="icon-back"></span></a>
            <div class="icon rounded-circle"></div>
            <h5 class="col-8">
                <a href="communityMembers.php">坂道遊戲群(190)</a>
                <div class="lock yes"></div>
            </h5>
            <a href="communitySetting.php" class="icon-setting col-2"></a>
        </header>
        <!-- content -->
        <main class="communityContentGroup">
            <div class="innerHeader">
                <div class="banner" style="background-image:url(styles/images/communityContent/10.jpg)"></div>
                <div class="member d-flex justify-content-between align-items-center">
                    <ul class="avaGroup d-flex justify-content-between align-items-center">
                        <li class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/1.jpg)"></li>
                        <li class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/2.jpg)"></li>
                        <li class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/3.jpg)"></li>
                        <li class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/4.jpg)"></li>
                        <li class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/5.jpg)"></li>
                    </ul>
                    <span class="number">等190名成員</span>
                    <a href="inviteMember.php" class="invite text-dark">邀請+</a>
                </div>
                <div class="release d-flex justify-content-start align-items-center">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/7.jpg)"></div>
                    <form action="">
                        <input type="text" class="text-dark f-12" placeholder="發布貼文...">
                    </form>
                </div>
            </div>
            <div class="innerContent">
                <form action="" class="postSection">
                    <a href="othersInfo.php" class="top">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/8.jpg)"></div>
                        <span class="name">MAYUYU(管理員)</span>
                    </a>
                    <div class="mid">
                        <div class="title">90823 STARGROUP-星族文化</div>
                        <div class="img" style="background-image:url(styles/images/communityContent/10.jpg)"></div>
                    </div>
                    <div class="icon-active-area color-light icon w-100 d-flex justify-content-end align-items-center">
                        <button type="button" class="icon-heart"></button>
                        <button type="button" class="icon-gift"></button>
                        <button type="button" class="icon-share"></button>
                        <button type="button" class="icon-collection"></button>
                    </div>
                    <div class="message-wrapper">
                        <input type="text" class="respond rounded-pill w-100 f-12" placeholder="留言回應....">
                        <button type="submit" class="sent-btn">
                            <span class="icon-sent-massage"></span>
                        </button>
                    </div>
                    <div class="msgGroup">
                        <div class="message icon-active-area color-dark w-100 d-flex justify-content-between align-items-center">
                            <a href="othersInfo.php" class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/1.jpg)"></a>
                            <div class="msg">
                                <span class="name">ten530712</span>
                                <span class="time">2小時前</span>
                                <div class="info"> 新曲的舞好難喔~但呈現出來很漂亮</div>
                            </div>
                            <button type="button" class="icon-unheart f-20"></button>
                        </div>
                    </div>
                </form>
                <form action="" class="postSection">
                    <a href="othersInfo.php" class="top">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/8.jpg)"></div>
                        <span class="name">MAYUYU(管理員)</span>
                    </a>
                    <div class="mid">
                        <div class="title">90823 STARGROUP-星族文化</div>
                        <div class="img" style="background-image:url(styles/images/communityContent/10.jpg)"></div>
                    </div>
                    <div class="icon-active-area color-light icon w-100 d-flex justify-content-end align-items-center">
                        <button type="button" class="icon-heart"></button>
                        <button type="button" class="icon-gift"></button>
                        <button type="button" class="icon-share"></button>
                        <button type="button" class="icon-collection"></button>
                    </div>
                    <div class="message-wrapper">
                        <input type="text" class="respond rounded-pill w-100 f-12" placeholder="留言回應....">
                        <button type="submit" class="sent-btn">
                            <span class="icon-sent-massage"></span>
                        </button>
                    </div>
                    <div class="msgGroup">
                        <div class="message icon-active-area color-dark w-100 d-flex justify-content-between align-items-center">
                            <a href="othersInfo.php" class="avatar rounded-circle" style="background-image:url(styles/images/communityContent/1.jpg)"></a>
                            <div class="msg">
                                <span class="name">ten530712</span>
                                <span class="time">2小時前</span>
                                <div class="info"> 新曲的舞好難喔~但呈現出來很漂亮</div>
                            </div>
                            <button type="button" class="icon-unheart f-20"></button>
                        </div>
                    </div>
                </form>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>