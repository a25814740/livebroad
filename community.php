<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="community">
            <div class="col-2"></div>
            <h3 class="col-8">社群</h3>
            <a href="myCommunity.php" class="my"></a>
            <a href="createCommunity.php" class="add col-2"></a>
        </header>
        <!-- content -->
        <main class="communityGroup">
            <div class="innerHeader">
                <form action="">
                    <div class="custom-search">
                        <input type="text" class="custom-search-input" required>
                        <input type="submit" value="">
                        <span class="icon-search"></span>
                    </div>
                </form>
            </div>
            <div class="innerContent">
                <ul class="menu nav nav-fill" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a href="#recommend" class="nav-link active" id="recommend-tab" data-toggle="pill">推薦</a>
                    </li>
                    <li class="nav-item">
                        <a href="#hotSearch" class="nav-link" id="hotSearch-tab" data-toggle="pill">熱搜</a>
                    </li>
                    <li class="nav-item">
                        <a href="#highPopularity" class="nav-link" id="highPopularity-tab" data-toggle="pill">高人氣</a>
                    </li>
                    <li class="nav-item">
                        <a href="#mostActive" class="nav-link" id="mostActive-tab" data-toggle="pill">最活躍</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div id="recommend" class="tab-pane fade show active" role="tabpanel">
                        <div class="group row no-gutters">
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">IZ*ONE</span>
                                <div class="avatar" style="background-image:url(styles/images/community/1.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">乃木坂46討論</span>
                                <div class="avatar" style="background-image:url(styles/images/community/2.jpeg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">高雄美食分享版</span>
                                <div class="avatar" style="background-image:url(styles/images/community/3.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">便宜旅遊</span>
                                <div class="avatar" style="background-image:url(styles/images/community/4.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">每周運勢</span>
                                <div class="avatar" style="background-image:url(styles/images/community/5.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">揪團購物</span>
                                <div class="avatar" style="background-image:url(styles/images/community/6.jpg)"></div>
                            </a>
                        </div>
                        <div id="communityGroup1">
                            <div class="d-flex justify-content-end align-items-center">
                                <button type="button" class="more" data-toggle="collapse" data-target="#innerCommunity1">查看更多<span class="icon-arrow-down f-12"></span></button>
                            </div>
                            <ul class="innerCommunity collapse" id="innerCommunity1" data-parent="#communityGroup1">
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/5.jpg)"></div>
                                        <span class="name">毛孩子交流(148位成員)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/6.jpg)"></div>
                                        <span class="name">福利熊，熊福利(378位成員)</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>






                    <div id="hotSearch" class="tab-pane fade" role="tabpanel">
                        <div class="group row no-gutters">
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">IZ*ONE</span>
                                <div class="avatar" style="background-image:url(styles/images/community/1.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">高雄美食分享版</span>
                                <div class="avatar" style="background-image:url(styles/images/community/3.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">乃木坂46討論</span>
                                <div class="avatar" style="background-image:url(styles/images/community/2.jpeg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">每周運勢</span>
                                <div class="avatar" style="background-image:url(styles/images/community/5.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">便宜旅遊</span>
                                <div class="avatar" style="background-image:url(styles/images/community/4.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">揪團購物</span>
                                <div class="avatar" style="background-image:url(styles/images/community/6.jpg)"></div>
                            </a>
                        </div>
                        <div id="communityGroup2">
                            <div class="d-flex justify-content-end align-items-center">
                                <button type="button" class="more" data-toggle="collapse" data-target="#innerCommunity2">查看更多<span class="icon-arrow-down f-12"></span></button>
                            </div>
                            <ul class="innerCommunity collapse" id="innerCommunity2" data-parent="#communityGroup2">
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/5.jpg)"></div>
                                        <span class="name">毛孩子交流(148位成員)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/6.jpg)"></div>
                                        <span class="name">福利熊，熊福利(378位成員)</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>






                    <div id="highPopularity" class="tab-pane fade" role="tabpanel">
                        <div class="group row no-gutters">
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">每周運勢</span>
                                <div class="avatar" style="background-image:url(styles/images/community/5.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">揪團購物</span>
                                <div class="avatar" style="background-image:url(styles/images/community/6.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">IZ*ONE</span>
                                <div class="avatar" style="background-image:url(styles/images/community/1.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">乃木坂46討論</span>
                                <div class="avatar" style="background-image:url(styles/images/community/2.jpeg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">高雄美食分享版</span>
                                <div class="avatar" style="background-image:url(styles/images/community/3.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">便宜旅遊</span>
                                <div class="avatar" style="background-image:url(styles/images/community/4.jpg)"></div>
                            </a>
                        </div>
                        <div id="communityGroup3">
                            <div class="d-flex justify-content-end align-items-center">
                                <button type="button" class="more" data-toggle="collapse" data-target="#innerCommunity3">查看更多<span class="icon-arrow-down f-12"></span></button>
                            </div>
                            <ul class="innerCommunity collapse" id="innerCommunity3" data-parent="#communityGroup3">
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/5.jpg)"></div>
                                        <span class="name">毛孩子交流(148位成員)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/6.jpg)"></div>
                                        <span class="name">福利熊，熊福利(378位成員)</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>





                    <div id="mostActive" class="tab-pane fade" role="tabpanel">

                        <div class="group row no-gutters">
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">揪團購物</span>
                                <div class="avatar" style="background-image:url(styles/images/community/6.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">高雄美食分享版</span>
                                <div class="avatar" style="background-image:url(styles/images/community/3.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">IZ*ONE</span>
                                <div class="avatar" style="background-image:url(styles/images/community/1.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4 hotItem">
                                <span class="icon-hot"></span>
                                <span class="title">乃木坂46討論</span>
                                <div class="avatar" style="background-image:url(styles/images/community/2.jpeg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">便宜旅遊</span>
                                <div class="avatar" style="background-image:url(styles/images/community/4.jpg)"></div>
                            </a>
                            <a href="communityIntroduction.php" class="item col-4">
                                <span class="title">每周運勢</span>
                                <div class="avatar" style="background-image:url(styles/images/community/5.jpg)"></div>
                            </a>
                        </div>
                        <div id="communityGroup4">
                            <div class="d-flex justify-content-end align-items-center">
                                <button type="button" class="more" data-toggle="collapse" data-target="#innerCommunity4">查看更多<span class="icon-arrow-down f-12"></span></button>
                            </div>
                            <ul class="innerCommunity collapse" id="innerCommunity4" data-parent="#communityGroup4">
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/5.jpg)"></div>
                                        <span class="name">毛孩子交流(148位成員)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="communityIntroduction.php" class="comminityLink">
                                        <div class="avatar rounded-circle" style="background-image:url(styles/images/community/6.jpg)"></div>
                                        <span class="name">福利熊，熊福利(378位成員)</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>