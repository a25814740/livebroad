<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>上傳連結</h3>
        </header>
        <!-- content -->
        <main class="uploadLinkGroup">
            <form action="" class="innerContainer">
                <div class="form-group black row no-gutters justify-content-start align-items-center">
                    <label for="">影片連結：</label>
                    <input type="text" class="col ml-3">
                </div>
                <div class="form-group black row no-gutters justify-content-start align-items-center">
                    <label for="">影片標題：</label>
                    <input type="text" class="col ml-3">
                </div>
                <div class="form-group black row no-gutters flex-column justify-content-center align-items-start">
                    <label for="" class="mb-1">影片內文：</label>
                    <textarea name="" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group black row no-gutters justify-content-start align-items-center">
                    <label for="">設定額度：</label>
                    <div class="select ml-3">
                        <select name="amount" id="">
                            <option value="10鑽" selected>10鑽</option>
                            <option value="20鑽">20鑽</option>
                            <option value="30鑽">30鑽</option>
                            <option value="40鑽">40鑽</option>
                            <option value="50鑽">50鑽</option>
                            <option value="60鑽">60鑽</option>
                            <option value="70鑽">70鑽</option>
                            <option value="80鑽">80鑽</option>
                            <option value="90鑽">90鑽</option>
                            <option value="100鑽">100鑽</option>
                        </select>
                    </div>
                </div>
                <div class="btnGroup d-flex justify-content-between align-items-center mt-5">
                    <input type="submit" value="確定上傳" class="confirm rounded-pill">
                    <button onclick="window.history.go(-1);" type="button" class="cancel rounded-pill">取消上傳</button>
                </div>
            </form>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>