<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="friendType">
            <a href="friend.php" class="col-2"><span class="icon-back"></span></a>
            <h5 class="col-8 f-24">遊戲<span class="f-12">(類別)</span></h5>
            <a href="friendInvite.php" class="add"></a>
            <a href="friendDelete.php" class="trash"></a>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="friendTypeGroup">
            <div class="innerContainer">
                <ul>
                    <li>
                        <a href="othersInfo.php">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/9.jpg)"></div>
                            <span class="name">米莎前輩</span>
                        </a>
                    </li>
                    <li>
                        <a href="othersInfo.php">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/3.jpg)"></div>
                            <span class="name">仁美</span>
                        </a>
                    </li>
                    <li>
                        <a href="othersInfo.php">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/7.jpg)"></div>
                            <span class="name">平手友梨奈</span>
                        </a>
                    </li>
                    <li>
                        <a href="othersInfo.php">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/8.jpg)"></div>
                            <span class="name">栗子</span>
                        </a>
                    </li>
                </ul>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>