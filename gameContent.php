<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="recordContent map">
            <div class="col-2"></div>
            <div class="icon rounded-circle"></div>
            <h3 class="col-8">英雄聯盟</h3>
            <div class="avatar icon rounded-circle"></div>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="recordContentGroup">
            <form action="">
                <ul class="innerContent">
                    <li class="typeGroup mb-2">
                        <span class="title">類別:</span>
                        <span class="type">遊戲</span>
                    </li>
                    <li class="banner" style="background-image:url(styles/images/mapContent/10.jpg)"></li>
                    <li class="contentGroup mt-2">
                        <p class="title">詳細內容:</p>
                        <div class="text">246等/鑽石一/全角色/BO5中徵求一起爬分者!!</div>
                    </li>
                    <li class="timeGroup mt-2">
                        <span class="title">結束時間:</span>
                        <span class="date">2019/09/06</span>
                        <span class="time">11:30</span>
                    </li>
                    <li class="moneyGroup mt-2">
                        <span class="title">支付額度:</span>
                        <span class="money">10鑽</span>
                        <div class="red"></div>
                    </li>
                    <li class="peopleGroup">
                        <a href="confirmContent.php">
                            <div class="app">
                                <p class="number">已申請人數:4</p>
                                <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/mapContent/1.jpg)"></div>
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/mapContent/2.jpg)"></div>
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/mapContent/3.jpg)"></div>
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/mapContent/4.jpg)"></div>
                                </div>
                            </div>
                            <div class="add">
                                <p class="number">已確認人數:1</p>
                                <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/mapContent/5.jpg)"></div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <div class="btnGroup d-flex justify-content-between align-items-center">
                        <input type="submit" value="確定" class="confirm rounded-pill">
                        <a href="map.php" class="cancel rounded-pill">取消</a>
                    </div>
                </ul>
            </form>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>