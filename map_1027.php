<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="Map" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"/>
    <link rel="stylesheet" href="scripts/plugins/layui/css/layui.css"/>
    <link rel="stylesheet" href="https://i.icomoon.io/public/180a1a2f68/livebroad/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.css"/>
    <link rel="stylesheet" href="css/inlife.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper/js/swiper.min.js"> </script>
    <script src="scripts/plugins/layui/layui.all.js"></script>
    <!-- <script src="scripts/plugins/dotdotdot-js-master/dist/dotdotdot.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js"></script>
    <script src="scripts/default/main.js"></script>
    <script>
        var $window = $(window),
            body = $('body');
        function deviceW() {
            if ($window.width() > 769) {
                $('#main-wrapper').hide();
                $('#error-width').css('display', 'flex');

            } else {
                $('#main-wrapper').show();
                $('#error-width').hide();
                $('#error-width').css('display', 'none');
            }
        }
        deviceW()
        $window.on('resize', function () {
            setTimeout(() => {
                deviceW()
            }, 10);
        })
    </script>
    <style>
        /* Optional: Makes the sample page fill the window. */
        html, body, form, #map {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        var infowindow;
        var map, marker, infoBubble;
        window.onload = function () {
            var startPos;
            var geoSuccess = function (position) {
                startPos = position;
                initMap(parseFloat(startPos.coords.latitude), parseFloat(startPos.coords.longitude), 15);
                var clientvalue = document.getElementById("clientvalue");
                clientvalue.value = parseFloat(startPos.coords.latitude) + "," + parseFloat(startPos.coords.longitude);
            };
            var geoError = function (position) {
                console.log('Error occurred. Error code: ' + error.code);
                // error.code can be:
                //   0: unknown error
                //   1: permission denied
                //   2: position unavailable (error response from location provider)
                //   3: timed out
            };
            navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
        };
        function initMap(latitude, longitude, zoom) {
            var myStyles = [
                {
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [
                        { visibility: "off" }
                    ]
                }
            ];
            var myLatlng = new google.maps.LatLng(latitude, longitude);
            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatlng,
                zoom: zoom,
                gestureHandling: 'greedy',
                styles: myStyles,
                disableDefaultUI: true
            });
            SaveLocation(latitude, longitude);
            AddMark(map);
        }
        function SaveLocation(latitude, longitude) {
            //用jQuery的Ajax，GetData.ashx要求座標資料
            //更多用法請看jQuery API
            $.ajax({
                url: '/SaveLocation.ashx',
                type: 'post',
                data: {
                    latitude: latitude,
                    longitude: longitude
                },
                success: function (data, textStatus) {

                }
            });
        }
        //參數map為地圖的實體
        function AddMark(map) {
            //用jQuery的Ajax，GetData.ashx要求座標資料
            //更多用法請看jQuery API
            $.ajax({
                url: '/GetData.ashx',
                type: 'GET',
                data: {},
                dataType: 'json',
                success: function (data, textStatus) {
                    for (var item in data) {
                        //建立經緯度座標
                        var myLatlng = new google.maps.LatLng(data[item].location_latitude, data[item].location_longitude);
                        //加一個Marker到map中
                        marker = new RichMarker({
                            map: map,
                            position: myLatlng,
                            shadow: 'none',
                            content: '<div class="mapType" data-type="social">'+
                                        '<button type="button" class="group gameGroup test2">'+
                                            '<div class="avatar rounded-circle" style="background-image:url("' + data[item].user_img + '")"></div>' +
                                            '<div class="game rounded-circle type"></div>'+
                                        '</button>'+
                                    '</div>'
                                //'<div class="markericon"> ' +
                                //        '<img src="' + data[item].user_img + '"/>' +
                                //     '</div>' +
                                //     '<div class="mymarker"> ' +
                                //        '<img src="' + data[item].user_img + '"/>' +
                                //     '</div>'
                        });

                        AddInfo(map, marker, data[item].user_name, data[item].user_img, data[item].user_id);
                    }
                }
            });
        }
        //另外呼叫加入InfoWindow避免問題
        function AddInfo(map, marker, user_name, user_img, user_id) {
            var contentString =
" <div> " +
    " <a href='/Friend?userid=" + user_id + "'>" +
        " <div class='infoimg'> " +
        "   <img src='" + user_img + "'/>" +
        " </div>" +
    " </a>" +
    " <div class='infotext'> " +
        " <div>" +
            " <h5 style='margin: 5px 0 0 0; color:white';>" + user_name + "</h5> " +
        " </div>" +
        " <div>" +
            " <h6 style='margin: 5px 0 0 0; color:white';>個人狀態顯示區域</h6>" +
        " </div>" +
    " </div>" +
" </div>";
            infoBubble = new InfoBubble({
                map: map,
                position: marker.position,
                padding: 0,
                backgroundColor: 'black',
                borderRadius: 20,
                borderWidth: 2,
                borderColor: 'gold',
                arrowSize: 0,
                minHeight: 54,
                minWidth: 250,
                hideCloseButton: true,
                shadowStyle: 3
            });
            google.maps.event.addListener(marker, 'click', function () {
                infoBubble.setContent(contentString);
                infoBubble.setBubbleOffset(30, -10);
                infoBubble.close();
                infoBubble.open(map, marker);
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnJsmmbP18wXEiUPyqIXDfA701S8oTf7I"></script>
    <%--richmarker必須在GoogleMapAPI載入後再載入--%>
    <script src="../Scripts/richmarker.js"></script>
    <script src="../Scripts/infobubble.js"></script>
</head>
<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="sorting">
            <div class="select black">
                <select name="amount" id="selectType">
                    <option value="now">即時性</option>
                    <option value="community">社群</option>
                    <option value="activity">活動</option>
                    <option value="social" selected>社交</option>
                    <option value="game">遊戲</option>
                    <option value="profession">專業</option>
                </select>
            </div>
            <div class="header-right">
                <button type="button" class="no-bg-button sidebar-click-btn">
                    <span class="icon-menu f-38"></span>
                </button>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar mapSidebar">
            <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                <span class="icon-menu f-38"></span>
            </button>
            <div class="accordion" id="accordionExample">
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingOne">
                        <h5 class="mb-0">
                            <button class="mapBtn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                選擇分類
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <ul class="mapCardBody">
                            <li>
                                <a href=>
                                    <div class="icon fri"></div>
                                    <span class="ml-2">好友</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon per"></div>
                                    <span class="ml-2">追蹤</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon hot"></div>
                                    <span class="ml-2">熱門</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon loc"></div>
                                    <span class="ml-2">附近</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingTwo">
                        <h5 class="mb-0">
                            <a href="mapSorting.php" class="mapBtn collapsed d-block">
                                順序排列
                            </a>
                        </h5>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingThree">
                        <h5 class="mb-0">
                            <a href="activityRecord.php" class="mapBtn collapsed d-block">
                                活動紀錄
                            </a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->
        <main class="mapGroup">
            <div id="map" class="map"></div>
            <div class="mapType" data-type="social">
                <button type="button" class="group gameGroup test2">
                    <a href="othersInfo.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">秋元真夏</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="activity">
                <button type="button" class="group gameGroup test2">
                    <a href="activityContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">開學季市集</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="now">
                <button type=" button" class="group gameGroup test2">
                    <a href="nowContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">需要外送飲料</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="profession">
                <button type="button" class="group gameGroup test2">
                    <a href="professionContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">徵求日文家教</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="community">
                <button type=" button" class="group gameGroup test2">
                    <a href="communityIntroduction.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">寶可夢討論群</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <div class="mapType" data-type="game">
                <button type="button" class="group gameGroup test2">
                    <a href="gameContent.php" class="person rounded-pill d-flex justify-content-start align-items-center">
                        <div class="smallAvatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                        <ul class="info ml-2">
                            <li class="title">英雄聯盟</li>
                            <li class="time">2019/9/6 11:30</li>
                        </ul>
                    </a>
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/map/4.jpg)"></div>
                    <div class="game rounded-circle type"></div>
                </button>
            </div>
            <button class="location" type="button"></button>
        </main>
        <footer>
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a class="nav-link f-32 active" href="MyPage">
                        <span class="icon-search1"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link f-32" href="Message">
                        <span class="icon-messge"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link f-32" href="map">
                        <span class="icon-main"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link f-32" href="GroupMenu">
                        <span class="icon-community"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link f-32 " href="Videos?STATUS=A">
                        <span class="icon-video"></span>
                    </a>
                </li>
            </ul>
        </footer>
    </div>
</body>
    <script>
    $(document).ready(function() {
        // 初始
        $('#selectType option').each(function () {
            if ( $(this).is(':selected') ) {
                var selectVal = $(this).val();
                console.log(selectVal);
                $('[data-type=' + selectVal + ']').show();
            } else {
                return false;
            }
        })
        $('.gameGroup').on('click', function() {
            $(this).find('.person').toggleClass('active');
        })
        $('#selectType').on('change', function() {
            var thisType = $(this).val();
            console.log(thisType);
            switch (thisType) {
                case '社交':
                    $('.mapType').hide();
                    $('[data-type=social]').show();
                    break;
                case '活動':
                    $('.mapType').hide();
                    $('[data-type=activity]').show();
                    break;
                case '即時性':
                    $('.mapType').hide();
                    $('[data-type=now]').show();
                    break;
                case '專業':
                    $('.mapType').hide();
                    $('[data-type=profession]').show();
                    break;
                case '社群':
                    $('.mapType').hide();
                    $('[data-type=community]').show();
                    break;
                case '遊戲':
                    $('.mapType').hide();
                    $('[data-type=game]').show();
                    break;

                default:
                    break;
            }
        })
    });
</script>
</html>
