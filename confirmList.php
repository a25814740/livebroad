<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="javascript:window.history.back();" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">確認名單</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="confirmListGroup">
            <div class="innerContainer">
                <ul class="nav justify-content-between align-items-center pb-2" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="application-tab" data-toggle="pill" href="#application-content" role="tab">已申請者</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="confirm-tab" data-toggle="pill" href="#confirm-content" role="tab">已確認者</a>
                    </li>
                </ul>
                <div class="tab-content pt-4" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="application-content" role="tabpanel">
                        <ul>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-between align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/3.jpg)"></div>
                                    <div class="info d-flex justify-content-start align-items-end">
                                        <span class="name">米莎前輩</span>
                                        <span class="account">@misa_46</span>
                                    </div>
                                    <div class="money d-flex justify-content-start align-items-center">
                                        <div class="red"></div>
                                        <span class="number">50</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-between align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/9.jpg)"></div>
                                    <div class="info d-flex justify-content-start align-items-end">
                                        <span class="name">仁美</span>
                                        <span class="account">@ninme</span>
                                    </div>
                                    <div class="money d-flex justify-content-start align-items-center">
                                        <div class="red"></div>
                                        <span class="number">50</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="confirm-content" role="tabpanel">
                        <ul>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-between align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/3.jpg)"></div>
                                    <div class="info d-flex justify-content-start align-items-end">
                                        <span class="name">米莎前輩</span>
                                        <span class="account">@misa_46</span>
                                    </div>
                                    <div class="money d-flex justify-content-start align-items-center">
                                        <div class="red"></div>
                                        <span class="number">50</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-between align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/9.jpg)"></div>
                                    <div class="info d-flex justify-content-start align-items-end">
                                        <span class="name">仁美</span>
                                        <span class="account">@ninme</span>
                                    </div>
                                    <div class="money d-flex justify-content-start align-items-center">
                                        <div class="red"></div>
                                        <span class="number">50</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>