
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Live Broadcast</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"> 
<link rel="stylesheet" href="scripts/plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" href="scripts/plugins/layui/css/layui.css">

<!-- <link rel="stylesheet" href="https://i.icomoon.io/public/temp/052aba2a63/livebroad/style.css"> -->
<link rel="stylesheet" href="styles/fonts/icomoon/style.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.css">

<link rel="stylesheet" href="styles/css/style.css">