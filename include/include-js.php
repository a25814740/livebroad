<div id="error-width" class="w-100 h-100 justify-content-center align-items-center">
    <p>請縮放到平板768px以下解析度</p>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"> </script>
<script src="scripts/plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="scripts/plugins/layui/layui.all.js"></script>
<script src="scripts/plugins/dotdotdot-js-master/dist/dotdotdot.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js"></script>
<script src="scripts/default/main.js"></script>

<script>
    
    var $window = $(window),
        body = $('body');
    function deviceW() {
        if( $window.width() > 769 ) {
            $('#main-wrapper').hide();
            $('#error-width').css('display', 'flex');
            
        } else {
            $('#main-wrapper').show();
            $('#error-width').hide();
            $('#error-width').css('display', 'none');
        }
    }
    deviceW()
    $window.on('resize', function() {
        setTimeout(() => {
            deviceW()
        }, 10);
    })

</script>