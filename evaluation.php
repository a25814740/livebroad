<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>我的評價</h3>
        </header>
        <!-- content -->
        <main class="evaluationGroup">
            <div class="innerContainer">
                <div class="evaluation">
                    <ul class="nav nav-tabs justify-content-between align-items-center" id="nav-tab" role="tablist">
                        <li class="nav-item">
                            <a href="#evaluation-content1" class="nav-link active" id="evaluation-tab1" data-toggle="pill">個人評價</a>
                        </li>
                        <li class="nav-item">
                            <a href="#evaluation-content2" class="nav-link" id="evaluation-tab2" data-toggle="pill">活動評價</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="evaluation-content1">
                            <ul>
                                <li>
                                    <div class="info">
                                        <p class="type">身分識別:4星</p>
                                        <span class="number">目前9次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">信用:2星</p>
                                        <span class="number">目前4次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">行為:3星</p>
                                        <span class="number">目前6次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">專業能力:4星</p>
                                        <span class="number">目前17次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">服務滿意:4星</p>
                                        <span class="number">目前26次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li class="d-flex justify-content-center align-items-center">
                                    <button class="rounded-pill" type="button">查看各項評價</button>
                                </li>
                            </ul>

                        </div>
                        <div class="tab-pane fade" id="evaluation-content2">
                            <ul>
                                <li>
                                    <div class="info">
                                        <p class="type">身分識別:4星</p>
                                        <span class="number">目前9次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">信用:2星</p>
                                        <span class="number">目前4次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">行為:3星</p>
                                        <span class="number">目前6次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">專業能力:4星</p>
                                        <span class="number">目前17次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <p class="type">服務滿意:4星</p>
                                        <span class="number">目前26次評價</span>
                                    </div>
                                    <div class="star">
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img yes"></div>
                                        <div class="img no"></div>
                                    </div>
                                </li>
                                <li class="d-flex justify-content-center align-items-center">
                                    <button class="rounded-pill" type="button">查看各項評價</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>