<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="community.php" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">創建社群</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="createCommunityGroup">
            <form action="community.php" class="innerContainer">
                <div class="openBtn form-inline">
                    <span>公開設定</span>
                    <label class="setBtn ml-3">
                        <input type="checkbox" checked>
                        <span class="slider"></span>
                    </label>
                </div>
                <div class="name formGroup">
                    <label for="">名稱：</label>
                    <input type="text">
                </div>
                <div class="uploadImgGroup formGroup">
                    <label for="">首頁圖片：</label>
                    <input type="file">
                    <div class="uploadImg">
                        <div class="imgLabel"></div>
                    </div>
                </div>
                <div class="info black formGroup">
                    <label for="">介紹內文：</label>
                    <textarea name="" id="" cols="30" rows="10"></textarea>
                </div>
                <span class="invite formGroup">
                    <span class="w-100">
                        <a href="inviteMember.php" class="d-flex align-items-center">
                            <span>邀請成員</span>
                            <span class="icon-arrow-right ml-1 f-14"></span>
                        </a>
                    </span>
                </span>
                <div class="btnGroup d-flex justify-content-between align-items-center">
                    <input type="submit" value="確定創建" class="confirm rounded-pill">
                    <a href="community.php" class="cancel rounded-pill">取消創建</a>
                </div>
            </form>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>