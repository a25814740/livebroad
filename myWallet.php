<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myWallet flex-column">
            <h3>我的錢包</h3>
            <div class="innerHeader w-100 d-flex justify-content-between align-items-center no-gutters">
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/gift.svg)"></div>
                    <div class="info">
                        <p class="title">我的禮點</p>
                        <p>59347</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/diamond.svg)"></div>
                    <div class="info">
                        <p class="title">我的鑽石</p>
                        <p>3394</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/money.svg)"></div>
                    <div class="info">
                        <p class="title">我的現金</p>
                        <p>70346</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- content -->
        <main class="myWalletGroup">
            <ul class="innerContent">
                <li>
                    <a href="user.php">收入明細<span class="icon-next f-14"></span></a>
                </li>
                <li>
                    <a href="expenditure.php">支出明細<span class="icon-next f-14"></span></a>
                </li>
                <li>
                    <a href="javascript:;">提現<span class="icon-next f-14"></span></a>
                </li>
                <li>
                    <a href="exchange.php">換匯<span class="icon-next f-14"></span></a>
                </li>
                <li>
                    <a href="stored.php">儲值鑽石<span class="icon-next f-14"></span></a>
                </li>
                <li>
                    <a href="operators.php">管理者<span class="icon-next f-14"></span></a>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>