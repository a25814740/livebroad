<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myWallet flex-column">
            <div class="w-100 d-flex justify-content-between align-items-center">
                <a href="myWallet.php" class="col-2"><span class="icon-back"></span></a>
                <h3 class="col-8">收入明細</h3>
                <div class="col-2"></div>
            </div>
            <div class="innerHeader w-100 d-flex justify-content-between align-items-center no-gutters">
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/gift.svg)"></div>
                    <div class="info">
                        <p class="title">我的禮點</p>
                        <p>59347</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/diamond.svg)"></div>
                    <div class="info">
                        <p class="title">我的鑽石</p>
                        <p>3394</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/money.svg)"></div>
                    <div class="info">
                        <p class="title">我的現金</p>
                        <p>70346</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- content -->
        <main class="myWalletGroup">
            <div class="navGroup d-flex justify-content-between align-items-center">
                <ul class="nav" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="person-tab" data-toggle="pill" href="#person-content" role="tab">個人<br>收入</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="ad-tab" data-toggle="pill" href="#ad-content" role="tab">廣告<br>收入</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="recommend-tab" data-toggle="pill" href="#recommend-content" role="tab">推薦<br>紀錄</a>
                    </li>
                </ul>
                <form action="" class="custom-search">
                    <input type="text" class="custom-search-input" required>
                    <input type="submit" value="">
                    <span class="icon-search"></span>
                </form>
            </div>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active personalIncome" id="person-content" role="tabpanel">
                    <?php include('userPersonalIncome.php') ?>
                </div>
                <div class="tab-pane fade ad" id="ad-content" role="tabpanel">
                    <?php include('userAdIncome.php') ?>
                </div>
                <div class="tab-pane fade recommend" id="recommend-content" role="tabpanel">
                    <?php include('userRecommendedRecord.php') ?>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        $('.recommendLink').on('click', function() {
            var type = $(this).attr('data-type');
            if (type == 'myOperators') {
                $('.invite').show();
            } else {
                $('.invite').hide();
            }
        });
        $('.changeSelect').on('click', function() {
            $(this).parents('.listContent').attr('data-select', 'true');
            $(this).parents('.reviewList').show();
        });
        $('.innerDel').on('click', function() {
            $(this).parents('.listContent').attr('data-select', 'false');
        });
        $('.distributionCheck').on('click', function() {
            if ($('.distributionContent').attr('data-check') == 'false') {
                $(this).parents('.distributionContent').attr('data-check', 'true');
                $(this).parents('.distributionList').show();
            } else {
                $(this).parents('.distributionContent').attr('data-check', 'false');
            }
        });
    });
</script>

</html>