<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <form action="">
            <!-- header -->
            <header class="friendCreateGroup">
                <a href="friend.php" class="col-2"><span class="icon-back"></span></a>
                <h3 class="col-8">創建類別</h3>
                <div class="col-2"></div>
                <button type="button" class="inviteBtn rounded-pill">創建</button>
            </header>
            <!-- content -->
            <main class="inviteMemberGroup friendCreateContent memberGroup">
                <div class="search d-flex justify-content-start align-items-center">
                    <label for="">類別名稱：</label>
                    <input type="text">
                </div>
                <div class="innerHeader mt-4">
                    <form action="">
                        <div class="custom-search">
                            <input type="text" class="custom-search-input" required>
                            <input type="submit" value="">
                            <span class="icon-search"></span>
                        </div>
                    </form>
                </div>
                <div class="innerContent">
                    <ul class="group">
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/1.jpg)"></div>
                            <label for="input1" class="col mb-0">麻友</label>
                            <input id="input1" type="checkbox" class="col-auto">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/2.jpg)"></div>
                            <label for="input2" class="col mb-0">咪醬</label>
                            <input id="input2" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/3.jpg)"></div>
                            <label for="input3" class="col mb-0">美金大</label>
                            <input id="input3" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/4.jpg)"></div>
                            <label for="input4" class="col mb-0">小楓</label>
                            <input id="input4" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/5.jpg)"></div>
                            <label for="input5" class="col mb-0">桃子</label>
                            <input id="input5" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/6.jpg)"></div>
                            <label for="input6" class="col mb-0">米莎前輩</label>
                            <input id="input6" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/7.jpg)"></div>
                            <label for="input7" class="col mb-0">仁美</label>
                            <input id="input7" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/8.jpg)"></div>
                            <label for="input8" class="col mb-0">平手友梨奈</label>
                            <input id="input8" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/9.jpg)"></div>
                            <label for="input9" class="col mb-0">栗子</label>
                            <input id="input9" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/8.jpg)"></div>
                            <label for="input10" class="col mb-0">平手友梨奈</label>
                            <input id="input10" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/9.jpg)"></div>
                            <label for="input11" class="col mb-0">栗子</label>
                            <input id="input11" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/8.jpg)"></div>
                            <label for="input12" class="col mb-0">平手友梨奈</label>
                            <input id="input12" class="col-auto" type="checkbox">
                        </li>
                        <li class="row no-gutters info">
                            <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendCreateType/9.jpg)"></div>
                            <label for="input13" class="col mb-0">栗子</label>
                            <input id="input13" class="col-auto" type="checkbox">
                        </li>
                    </ul>
                </div>
            </main>
        </form>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>