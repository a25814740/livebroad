<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="searchFriend">
            <a href="personInfo.php" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">搜尋好友</h3>
            <div class="col-2"></div>
            <div class="icon"></div>
        </header>
        <!-- content -->
        <main class="searchFriendGroup f-18">
            <div class="innerHeader">
                <div class="title">請輸入ID進行搜尋</div>
                <form action="">
                    <div class="custom-search">
                        <input type="text" class="custom-search-input" required>
                        <input type="submit" value="">
                        <span class="icon-search"></span>
                    </div>
                </form>
            </div>
            <div class="innerContent d-flex flex-column justify-content-center align-items-center">
                <div class="avatar rounded-circle"></div>
                <p class="name">橋本女士</p>
                <button type="button" class="personBtn rounded-pill">查看個人</button>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>