<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="mediaIndex.php" class="col-2"><span class="icon-back"></span></a>
            <div class="col-8"></div>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="dynamicGroup videoContent">
            <div class="section">
                <div class="innerContent">
                    <div class="video blur-wrapper">
                        <div class="image blur" style="background: url(https://img.youtube.com/vi/11-q5_U2li0/hqdefault.jpg) no-repeat center center/cover;"></div>
                        <!-- <div class="play"></div> -->
                        <button class="text-buble unlock layui-btn h-auto play-btn bg-transparent" id="layerUnlockBtn" type="button" data-method="unlock" data-type="auto">
                            <div class="play"></div>
                        </button>
                        <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/11-q5_U2li0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="innerContainer">
                        <div class="title">乃木坂46／Sing Out！(中文字幕完整版)</div>
                        <div class="innerText">
                            <div class="tagGroup">
                                <a href="javascript:;" class="tag">#Sing Out</a>
                                <a href="javascript:;" class="tag">#乃木坂46</a>
                            </div>
                            <p class="text">◎日本國民女子天團乃木坂46令和年號首發單曲！<br>
                                ◎齋藤飛鳥擔任CENTER，散播愛與歡樂的正能量金曲！<br>
                                ◎ORICON公信榜單曲綜合榜、實體單曲榜、數位專輯榜三榜冠軍！</p>
                        </div>
                        <div class="d-flex justify-content-between align-items-end mt-3">
                            <div class="icon d-flex justify-content-between align-items-center">
                                <button type="button" class="icon-heart"></button>
                                <button type="button" class="icon-gift"></button>
                                <button type="button" class="icon-share"></button>
                                <button type="button" class="icon-collection"></button>
                            </div>
                        </div>
                        <form action="" class="message-wrapper mt-3">
                            <input type="text" class="respond rounded-pill w-100 f-12" placeholder="留言回應....">
                            <button type="submit" class="sent-btn">
                                <span class="icon-sent-massage"></span>
                            </button>
                        </form>
                        <div class="msgGroup">
                            <div class="message icon-active-area color-dark w-100 d-flex justify-content-between align-items-center">
                                <a href="othersInfo.php" class="avatar rounded-circle" style="background-image:url(styles/images/videoContent/1.jpg)"></a>
                                <div class="msg">
                                    <span class="name">ten530712</span>
                                    <span class="time">2小時前</span>
                                    <div class="info"> 新曲的舞好難喔~但呈現出來很漂亮</div>
                                </div>
                                <button type="button" class="icon-unheart f-20"></button>
                            </div>
                            <div class="message icon-active-area color-dark w-100 d-flex justify-content-between align-items-center">
                                <a href="othersInfo.php" class="avatar rounded-circle" style="background-image:url(styles/images/videoContent/1.jpg)"></a>
                                <div class="msg">
                                    <span class="name">ten530712</span>
                                    <span class="time">2小時前</span>
                                    <div class="info"> 新曲的舞好難喔~但呈現出來很漂亮</div>
                                </div>
                                <button type="button" class="icon-unheart f-20"></button>
                            </div>
                            <div class="message icon-active-area color-dark w-100 d-flex justify-content-between align-items-center">
                                <a href="othersInfo.php" class="avatar rounded-circle" style="background-image:url(styles/images/videoContent/1.jpg)"></a>
                                <div class="msg">
                                    <span class="name">ten530712</span>
                                    <span class="time">2小時前</span>
                                    <div class="info"> 新曲的舞好難喔~但呈現出來很漂亮</div>
                                </div>
                                <button type="button" class="icon-unheart f-20"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>