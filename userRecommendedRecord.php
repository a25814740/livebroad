<ul class="nav" id="recommendContect-tab" role="tablist">
    <li class="nav-item mr-2">
        <a class="recommendLink nav-link active" id="myRecommend-tab" data-toggle="pill" href="#myRecommend-content" data-type="myRecommend" role="tab">我的推薦</a>
    </li>
</ul>
<div class="tab-content mt-3" id="recommendContect-tabContent">
    <div class="tab-pane fade show active review" id="myRecommend-content" role="tabpanel">
        <ul class="flex-ul">
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/1.jpg)"></div>
                    <div class="info">
                        <p class="account">仁美@manaya7320</p>
                        <p class="position smallText">高雄市，小港區</p>
                        <div class="text smallText">
                            <p>2019/08/13</p>
                            <p>19:32</p>
                        </div>
                    </div>
                </div>
                <button class="delBtn rounded-pill">刪除</button>
            </li>
        </ul>
        <div class="intro d-flex justify-content-between align-items-end">
            <p>推薦我</p>
            <span class="f-12">是否活躍</span>
        </div>
        <ul class="recommendMe flex-ul">
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/3.jpg)"></div>
                    <div class="info">
                        <p class="account">麻友@gcian_48</p>
                        <p class="position smallText">台南市，中西區</p>
                        <div class="text smallText">
                            <p>2019/09/03</p>
                            <p>07:11</p>
                        </div>
                    </div>
                </div>
                <button class="yes"></button>
            </li>
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/4.jpg)"></div>
                    <div class="info">
                        <p class="account">咪醬@minami_48</p>
                        <p class="position smallText">桃園市，大園區</p>
                        <div class="text smallText">
                            <p>2019/09/15</p>
                            <p>23:26</p>
                        </div>
                    </div>
                </div>
                <button class="no"></button>
            </li>
            <li>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="avatar rounded-circle" style="background-image:url(styles/images/wallet/5.jpg)"></div>
                    <div class="info">
                        <p class="account">美月@nogizaka_46</p>
                        <p class="position smallText">高雄市，前鎮區</p>
                        <div class="text smallText">
                            <p>2019/10/25</p>
                            <p>12:40</p>
                        </div>
                    </div>
                </div>
                <button class="yes"></button>
            </li>
        </ul>
    </div>
</div>