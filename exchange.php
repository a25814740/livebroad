<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myWallet flex-column">
            <div class="w-100 d-flex justify-content-between align-items-center">
                <a href="myWallet.php" class="col-2"><span class="icon-back"></span></a>
                <h3 class="col-8">換匯</h3>
                <div class="col-2"></div>
            </div>
            <div class="innerHeader w-100 d-flex justify-content-between align-items-center no-gutters">
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/gift.svg)"></div>
                    <div class="info">
                        <p class="title">我的禮點</p>
                        <p>59347</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/diamond.svg)"></div>
                    <div class="info">
                        <p class="title">我的鑽石</p>
                        <p>3394</p>
                    </div>
                </div>
                <div class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/money.svg)"></div>
                    <div class="info">
                        <p class="title">我的現金</p>
                        <p>70346</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- content -->
        <main class="exchangeGroup">
            <div class="innerContent">
                <ul class="nav mb-3" id="exchange-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="change-tab" data-toggle="pill" href="#change-content" role="tab">換匯</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a class="nav-link" id="history-tab" data-toggle="pill" href="#history-content" role="tab">換匯<br>紀錄</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="change-content" role="tabpanel">
                        <ul>
                            <li class="no-gutters mt-3 mb-3">
                                <div class="col-1">
                                    <div class="space"></div>
                                </div>
                                <div class="col-3">總數</div>
                                <div class="col-2 change">兌換數量</div>
                                <div class="col-1">
                                    <div class="space"></div>
                                </div>
                                <div class="col-3 pl-2">轉換後數量</div>
                                <div class="col-2"></div>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="gift"></div>
                                </div>
                                <div class="col-3 total">59347</div>
                                <div class="col-2 change color">3000</div>
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 changeNumber">1253.656</div>
                                <button type="button">兌換</button>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 total">70346</div>
                                <div class="col-2 change color">3000</div>
                                <div class="col-1">
                                    <div class="dim"></div>
                                </div>
                                <div class="col-3 changeNumber">4914</div>
                                <button type="button">兌換</button>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="dim"></div>
                                </div>
                                <div class="col-3 total">3394</div>
                                <div class="col-2 change color">3000</div>
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 changeNumber">1253.656</div>
                                <button type="button">兌換</button>
                            </li>
                        </ul>
                        <div class="innerContainer">
                            <ul class="note">
                                <li class="title">注意事項:</li>
                                <li>1、小數點後3位不計算</li>
                                <li>2、本平台退款方式直接用鑽石轉換成台幣後 提現，將收取匯差、人事成本、業務費用</li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="history-content" role="tabpanel">
                        <ul>
                            <li class="row no-gutters justify-content-between align-items-center mt-3 mb-3">
                                <div class="col-1">
                                    <div class="space"></div>
                                </div>
                                <div class="col-2 ml-3 change">兌換數量</div>
                                <div class="col-1">
                                    <div class="space"></div>
                                </div>
                                <div class="col-3">轉換後數量</div>
                                <div class="col-3">日期</div>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="gift"></div>
                                </div>
                                <div class="col-2 ml-3 change color">500</div>
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 changeNumber">190.621</div>
                                <div class="col-3 date">
                                    <span>2019/09/06</span>
                                    <span>07:15</span>
                                </div>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-2 ml-3 change color">100</div>
                                <div class="col-1">
                                    <div class="dim"></div>
                                </div>
                                <div class="col-3 changeNumber">163.8</div>
                                <div class="col-3 date">
                                    <span>2019/09/03</span>
                                    <span>13:48</span>
                                </div>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="dim"></div>
                                </div>
                                <div class="col-2 ml-3 change color">3000</div>
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 changeNumber">1253.656</div>
                                <div class="col-3 date">
                                    <span>2019/08/25</span>
                                    <span>18:46</span>
                                </div>
                            </li>
                            <li class="item no-gutters">
                                <div class="col-1">
                                    <div class="dim"></div>
                                </div>
                                <div class="col-2 ml-3 change color">5000</div>
                                <div class="col-1">
                                    <div class="money"></div>
                                </div>
                                <div class="col-3 changeNumber">2089.427</div>
                                <div class="col-3 date">
                                    <span>2019/08/11</span>
                                    <span>09:07</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>