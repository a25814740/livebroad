<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="createSettingGroup">
            <form action="" class="innerContent">
                <ul>
                    <li>
                        <label for="">創建類別:</label>
                        <div class="select black ml-3">
                            <select name="amount" id="">
                                <option value="活動" selected>活動</option>
                                <option value=""></option>
                                <option value=""></option>
                            </select>
                        </div>
                    </li>
                    <li class="title black">
                        <label for="">標題:</label>
                        <input class="col-7" type="text">
                        <span class="f-12 smallText">(10個字以內)</span>
                    </li>
                    <li class="textarea black flex-wrap">
                        <label for="">內文敘述:</label>
                        <textarea name="" id="" cols="30" rows="10"></textarea>
                    </li>
                    <li class="uploadImgGroup flex-wrap">
                        <label for="">上傳圖片：</label>
                        <input type="file">
                        <div class="uploadImg">
                            <div class="imgLabel"></div>
                        </div>
                    </li>
                    <li class="end black align-items-start">
                        <label for="">結束時間:</label>
                        <div class="ml-3">
                            <input type="date" id="inputDate">
                            <input type="time" id="inputTime">
                        </div>
                    </li>
                    <li>
                        <label for="">選擇需求:</label>
                        <div class="select black ml-3">
                            <select name="amount" id="">
                                <option value="收取" selected>收取</option>
                                <option value="付款">付款</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <label for="">設定額度:</label>
                        <div class="select black ml-3">
                            <select name="amount" id="">
                                <option value="10鑽" selected>10鑽</option>
                                <option value="20鑽">20鑽</option>
                                <option value="30鑽">30鑽</option>
                                <option value="40鑽">40鑽</option>
                                <option value="50鑽">50鑽</option>
                                <option value="60鑽">60鑽</option>
                                <option value="70鑽">70鑽</option>
                                <option value="80鑽">80鑽</option>
                                <option value="90鑽">90鑽</option>
                                <option value="100鑽">100鑽</option>
                            </select>
                        </div>
                    </li>
                </ul>
                <div class="btnGroup d-flex justify-content-between align-items-center mt-5">
                    <input type="submit" value="確定創建" class="confirm rounded-pill">
                    <button type="button" class="cancel rounded-pill">取消創建</button>
                </div>
            </form>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>