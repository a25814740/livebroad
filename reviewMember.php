<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="reviewMember">
            <a href="communitySetting.php" class="col-2"><span class="icon-back"></span></a>
            <div class="icon rounded-circle"></div>
            <h5 class="col-8">
                <a href="communityMembers.php">坂道遊戲群(190)</a>
            </h5>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="reviewMemberGroup">
            <div class="innerContent">
                <p class="title">審核成員</p>
                <ul class="group">
                    <li class="info d-flex justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/reviewMember/1.jpg)"></div>
                        <div class="ml-2">
                            <p class="name">咪醬</p>
                            <p class="status">申請加入社團</p>
                        </div>
                        <div class="reviewBtnGroup">
                            <button type="button" class="rounded-pill">接受</button>
                            <button type="button" class="rounded-pill">拒絕</button>
                        </div>
                    </li>
                    <li class="info d-flex justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/reviewMember/2.jpg)"></div>
                        <div class="ml-2">
                            <p class="name">ten醬</p>
                            <p class="status">申請加入社團</p>
                        </div>
                        <div class="reviewBtnGroup">
                            <button type="button" class="rounded-pill">接受</button>
                            <button type="button" class="rounded-pill">拒絕</button>
                        </div>
                    </li>
                    <li class="info d-flex justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/reviewMember/3.jpg)"></div>
                        <div class="ml-2">
                            <p class="name">桃子</p>
                            <p class="status">申請加入社團</p>
                        </div>
                        <div class="reviewBtnGroup">
                            <button type="button" class="rounded-pill">接受</button>
                            <button type="button" class="rounded-pill">拒絕</button>
                        </div>
                    </li>
                    <li class="info d-flex justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/reviewMember/4.jpg)"></div>
                        <div class="ml-2">
                            <p class="name">蘭世</p>
                            <p class="status">申請加入社團</p>
                        </div>
                        <div class="reviewBtnGroup">
                            <button type="button" class="rounded-pill">接受</button>
                            <button type="button" class="rounded-pill">拒絕</button>
                        </div>
                    </li>
                </ul>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>