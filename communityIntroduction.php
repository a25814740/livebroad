<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="communityIntroduction">
            <a href="javascript:window.history.back();" class="col-2"><span class="icon-back"></span></a>
            <div class="icon rounded-circle"></div>
            <h5 class="col-8">
                <a href="communityMembers.php">坂道遊戲群(190)</a>
            </h5>
            <div class="lock yes"></div>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="communityIntroductionGroup">
            <div class="innerHeader">
                <div class="banner" style="background-image:url(styles/images/communityIntroduction/10.jpg)"></div>
                <div class="attributes">
                    <span class="open">不公開社團</span>
                    <span class="number">190位成員</span>
                </div>
                <button type="button">加入社團</button>
            </div>
            <ul class="innerContent">
                <li class="dynamic">
                    <div class="title">社團動態</div>
                    <ul class="content">
                        <li>今天有4則新貼文</li>
                        <li>上週增加了9位成員</li>
                        <li>建立於2個月前</li>
                    </ul>
                </li>
                <li class="member">
                    <div class="title">社團成員</div>
                    <ul class="content">
                        <li class="d-flex justify-content-start align-items-center">
                            <div class="avaGroup d-flex align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/communityIntroduction/1.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/communityIntroduction/2.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/communityIntroduction/3.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/communityIntroduction/4.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/communityIntroduction/5.jpg)"></div>
                            </div>
                            <span class="number">等190位</span>
                        </li>
                        <li class="moderator">版主:<span>秋元真夏</span></li>
                    </ul>
                </li>
                <li class="introduction">
                    <div class="title">社團介紹</div>
                    <div class="content">此群和平不搶婆，可以曬卡，交流攻略要拉朋友入群請先告知</div>
                </li>
            </ul>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>