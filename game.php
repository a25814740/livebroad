<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Game</title>
    <link rel="stylesheet" href="https://meyerweb.com/eric/tools/css/reset/reset200802.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/layer/2.3/skin/layer.css">
    <style>
        html,
        body,
        iframe {
            width: 100%;
            height: 100%;
        }

        .back {
            position: fixed;
            top: 0;
            right: 0;
            background: #FFF;
            width: 35px;
            height: 35px;
            background: url('./styles/icons/game1-back.png') 18% 76% / cover no-repeat;
            background-size: 700px;
            background-position: -116px -46px;
            border: 0;
        }
    </style>
</head>

<body>
    <button class="back" onclick="goBack()"></button>
    <iframe id="game-iframe" src="http://h5play.com.tw/games/play/22" frameborder="0"></iframe>




    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/layer/2.3/layer.js"></script>
    <script>
        function goBack() {
            window.location.href = "./"
        }

        layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            skin: 'yourclass',
            content: $('#game-iframe'),
            success: function(layero, index){
                var body = layer.getChildFrame('body', index);
                var iframeWin = window[layero.find('iframe')[0]; 
                console.log(body.html()) 
                console.log(iframeWin.html()) 
            }
        });

        // var iframe = document.getElementById("game-iframe");
        // var elmnt = iframe.contentWindow.document.getElementById("move-dot");
        // elmnt.style.display = "none";
        // $('#game-iframe').contents().find('#move-dot').css('display', 'hidden');
    </script>
</body>

</html>