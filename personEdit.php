<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <form action="" class="w-100 h-100">
        <div id="main-wrapper" class="container-fluid p-0">
            <!-- header -->
            <div class="editHeader d-flex justify-content-between align-items-start">
                <a href="personInfo.php" class="allow"><span class="icon-back"></span></a>
                <input type="file" class="bgBtn">
                <div class="background"></div>
                <button type="button" class="editBtn rounded-pill" onclick="window.location.href='personInfo.php'">儲存</button>
            </div>
            <!-- content -->
            <main class="editPerson">
                <div class="avatar rounded-circle d-flex justify-content-center align-items-center">
                    <input type="file" class="avaBtn">
                </div>
                <div class="infoContainer h-100">
                    <div class="infoGroup d-flex flex-column justify-content-between align-items-center">
                        <div class="input">
                            <label for="input1">名稱</label>
                            <input id="input1" type="text">
                        </div>
                        <div class="input">
                            <label for="input2">ID設定</label>
                            <input id="input2" type="text">
                        </div>
                        <div class="input">
                            <label for="input3">自我介紹</label>
                            <textarea name="" id="input3" class="w-100 bg-transparent text-white"></textarea>
                        </div>
                        <div class="input">
                            <label for="input4">帳號信箱</label>
                            <input id="input4" type="text">
                        </div>
                        <div class="input">
                            <label for="input5">密碼更改</label>
                            <input id="input5" type="text">
                        </div>
                        <div class="input">
                            <label for="input6">再次確定密碼</label>
                            <input id="input6" type="text">
                        </div>
                    </div>
                    <div class="amount d-flex justify-content-start align-items-center">
                        <label class="m-0" for="input7"><span>陌生訊息額度設定</span></label>
                        <div class="select black ml-3">
                            <select name="amount" id="input7">
                                <option value="10鑽" selected>10鑽</option>
                                <option value="20鑽">20鑽</option>
                                <option value="30鑽">30鑽</option>
                                <option value="40鑽">40鑽</option>
                                <option value="50鑽">50鑽</option>
                                <option value="60鑽">60鑽</option>
                                <option value="70鑽">70鑽</option>
                                <option value="80鑽">80鑽</option>
                                <option value="90鑽">90鑽</option>
                                <option value="100鑽">100鑽</option>
                            </select>
                        </div>
                    </div>
                    <div class="openSet d-flex flex-column justify-content-between align-items-center">
                        <span>公開設定</span>
                        <div class="set">
                            <label class="m-0" for="input8"><span>性別</span></label>
                            <div class="sex d-flex">
                                <div class="form-check form-check-inline custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="inlineRadio2" name="radio-stacked" required>
                                    <label class="custom-control-label" for="inlineRadio2">男性</label>
                                </div>
                                <div class="form-check form-check-inline custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="inlineRadio1" name="radio-stacked" checked required>
                                    <label class="custom-control-label" for="inlineRadio1">女性</label>
                                </div>
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input9"><span>生日</span></label>
                            <div class="birthday d-flex justify-content-between align-items-center">
                                <input id="form-input5" type="text" class="col-8 form-control layui-input bg-transparent border-0" data-input-type="date" readonly>
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input10"><span>身高</span></label>
                            <div class="height d-flex justify-content-start align-items-center">
                                <input id="input10" type="text">
                                <span>公分</span>
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input11"><span>體重</span></label>
                            <div class="weight d-flex justify-content-start align-items-center">
                                <input id="input11" type="text">
                                <span>公斤</span>
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input12"><span>電話</span></label>
                            <div class="tel d-flex justify-content-start align-items-center">
                                <input id="input12" type="text">
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input13"><span>信箱</span></label>
                            <div class="mail d-flex justify-content-start align-items-center">
                                <input id="input13" type="text">
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set flex-wrap">
                            <label class="m-0" for="input14"><span>語言</span></label>
                            <button id="layerBtn1" type="button" data-method="offset" data-type="1" class="icon_plus col-auto layui-btn layui-btn-normal"></button>
                            <div class="language d-flex justify-content-start align-items-center p-0">
                                <input id="input14" type="text" class="col">
                                <label class="setBtn ml-auto">
                                    <input type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input15"><span>學歷</span></label>
                            <button id="layerBtn2" type="button" data-method="offset" data-type="2" class="icon_plus col-auto layui-btn layui-btn-normal"></button>
                            <div class="education d-flex justify-content-start align-items-center p-0">
                                <input id="input15" type="text" class="col">
                                
                                <label class="setBtn ml-auto">
                                    <input type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input16"><span>感情狀況</span></label>
                            <div class="affection d-flex justify-content-start align-items-center">
                                <select name="affection" id="" id="input16">
                                    <option value="單身">單身</option>
                                    <option value="單身">有伴侶</option>
                                </select>
                            </div>
                            <label class="setBtn">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                            </label>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input17"><span>職業</span></label>
                            <button type="button" class="icon_plus col-auto"></button>
                            <div class="career d-flex justify-content-start align-items-center p-0">
                                <input id="input17" type="text" class="col">
                                <label class="setBtn ml-auto">
                                    <input type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>
                        <div class="set">
                            <label class="m-0" for="input18"><span>專業</span></label>
                            <button type="button" class="icon_plus col-auto"></button>
                            <div class="profession d-flex justify-content-start align-items-center p-0">
                                <input id="input18" type="text" class="col">
                                <label class="setBtn ml-auto">
                                    <input type="checkbox" checked>
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </form>
    <?php include('include/include-js.php') ?>
</body>

</html>