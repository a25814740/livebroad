<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>好友</h3>
        </header>
        <!-- content -->
        <main class="friendGroup">
            <div class="innerHeader">
                <form action="">
                    <div class="custom-search">
                        <input type="text" class="custom-search-input" required>
                        <input type="submit" value="">
                        <span class="icon-search"></span>
                    </div>
                </form>
            </div>
            <div class="innerContainer">
                <ul class="nav justify-content-around align-items-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="friend-tab" data-toggle="pill" href="#friend-content" role="tab">好友</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="stranger-tab" data-toggle="pill" href="#stranger-content" role="tab">陌生人</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="friend-content" role="tabpanel">
                        <ul class="class">
                            <li class="title">
                                <a href="friendCreateType.php">
                                    <span class="f-18">類別</span>
                                    <span class="plus ml-2"></span>
                                </a>
                            </li>
                            <li>
                                <a href="friendType.php">遊戲(5)</a>
                            </li>
                            <li>
                                <a href="friendType.php">親戚家人(3)</a>
                            </li>
                            <li>
                                <a href="friendType.php">公司類(11)</a>
                            </li>
                        </ul>
                        <ul class="group">
                            <li class="title">
                                <a href="friendCreateType.php">
                                    <span class="f-18">群組</span>
                                    <span class="plus ml-2"></span>
                                </a>
                            </li>
                            <li>
                                <a href="friendGroup.php">寶可夢討論群(76)</a>
                            </li>
                            <li>
                                <a href="friendGroup.php">我們這一家(13)</a>
                            </li>
                            <li>
                                <a href="friendGroup.php">社團幹部群(26)</a>
                            </li>
                        </ul>
                        <ul class="people">
                            <li class="title">
                                <span class="f-18">好友</span>
                                <span class="space ml-2"></span>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/9.jpg)"></div>
                                    <span class="name">米莎前輩</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/3.jpg)"></div>
                                    <span class="name">仁美</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/7.jpg)"></div>
                                    <span class="name">平手友梨奈</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/8.jpg)"></div>
                                    <span class="name">栗子</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="stranger-content" role="tabpanel">
                        <ul class="class">
                            <li class="title">
                                <span class="f-18">類別</span>
                                <div class="space ml-2"></div>
                            </li>
                            <li>
                                <a href="friendType.php">遊戲(5)</a>
                            </li>
                            <li>
                                <a href="friendType.php">親戚家人(3)</a>
                            </li>
                            <li>
                                <a href="friendType.php">公司類(11)</a>
                            </li>
                        </ul>
                        <ul class="people">
                            <li class="title">
                                <span class="f-18">陌生人</span>
                                <div class="space ml-2"></div>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/9.jpg)"></div>
                                    <span class="name">米莎前輩</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/3.jpg)"></div>
                                    <span class="name">仁美</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/7.jpg)"></div>
                                    <span class="name">平手友梨奈</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/friend/8.jpg)"></div>
                                    <span class="name">栗子</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>