<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="register w-100 h-100">
            <form action="login.php" class="d-flex flex-column justify-content-center align-items-center">
                <div class="innerHeader d-flex flex-column justify-content-center align-items-center">
                    <div class="file">
                        <input type="file">
                    </div>
                    <span>上傳頭像</span>
                </div>
                <div class="innerContent">
                    <div class="form-group row rounded-pill">
                        <label for="form-input" class="col-5 col-form-label">使用者名稱</label>
                        <input id="form-input" class="input col-7 form-control" type="text" placeholder="">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input0" class="col-5 col-form-label">性別</label>
                        <div class="col-7 row no-gutters d-flex align-items-center">
                            <div class="form-check form-check-inline custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="inlineRadio1" name="radio-stacked" checked required>
                                <label class="custom-control-label" for="inlineRadio1">女</label>
                            </div>
                            <div class="form-check form-check-inline custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="inlineRadio2" name="radio-stacked" required>
                                <label class="custom-control-label" for="inlineRadio2">男</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input1" class="col-5 col-form-label">信箱</label>
                        <input id="form-input1" class="input col-7 form-control" type="email">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input2" class="col-5 col-form-label">密碼</label>
                        <input id="form-input2" class="input col-7 form-control" type="password">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input3" class="col-5 col-form-label">確認密碼</label>
                        <input id="form-input3" class="input col-7 form-control" type="password">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input4" class="col-5 col-form-label">自訂ID</label>
                        <input id="form-input4" class="input col-7 form-control" type="text">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input5" class="col-5 col-form-label">生日</label>
                        <input id="form-input5" type="date" class="col-7 form-control bg-transparent border-0 text-light">
                    </div>
                    <div class="form-group row rounded-pill">
                        <label for="form-input6" class="col-5 col-form-label">手機</label>
                        <input id="form-input6" class="input col-7 form-control" type="tel">
                    </div>
                    <div class="form-group">
                        <input class="confirm col-auto rounded-pill form-control" type="submit" value="確定註冊">
                    </div>
                </div>
            </form>
        </main>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>