<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="recordContent create">
            <a href="activityRecord.php" class="col-2"><span class="icon-back"></span></a>
            <div class="icon rounded-circle"></div>
            <h3 class="col-8">奇蹟暖暖</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="recordContentGroup">
            <ul class="innerContent">
                <li class="typeGroup mb-2">
                    <span class="title">類別:</span>
                    <span class="type">遊戲</span>
                </li>
                <li class="banner" style="background-image:url(styles/images/recordContent/10.jpg)"></li>
                <li class="contentGroup mt-2">
                    <p class="title">詳細內容:</p>
                    <div class="text">加入聯盟，解每日任務，貢獻值超過50即贈送100鑽!只限前5名</div>
                </li>
                <li class="timeGroup mt-2">
                    <span class="title">支付時間:</span>
                    <span class="date">2019/09/07</span>
                    <span class="time">23:28</span>
                </li>
                <li class="moneyGroup mt-2">
                    <span class="title">總支付額度:</span>
                    <span class="money">100鑽</span>
                    <div class="red"></div>
                </li>
                <li class="peopleGroup">
                    <a href="confirmContent.php">
                        <div class="app">
                            <p class="number">已申請人數:4</p>
                            <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/1.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/2.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/3.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/4.jpg)"></div>
                            </div>
                        </div>
                        <div class="add">
                            <p class="number">已確認人數:2</p>
                            <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/5.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/recordContent/6.jpg)"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="link">
                    <a href="giveEvaluation.php">前往評價<span class="icon-next f-14"></span></a>
                </li>
            </ul>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>