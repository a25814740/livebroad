<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="javascript:window.history.back();" class="col-2"><span class="icon-back"></span></a>
            <h5 class="col-8">確認名單</h5>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="confirmListGroup">
            <div class="innerContainer">
                <ul class="nav pb-2 justify-content-between align-items-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="application-tab" data-toggle="pill" href="#application-content" role="tab">已申請者</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="confirm-tab" data-toggle="pill" href="#confirm-content" role="tab">已確認者</a>
                    </li>
                </ul>
                <div class="tab-content pt-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="application-content" role="tabpanel">
                        <ul>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/9.jpg)"></div>
                                    <span class="name ml-4">米莎前輩</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/3.jpg)"></div>
                                    <span class="name ml-4">仁美</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/7.jpg)"></div>
                                    <span class="name ml-4">平手友梨奈</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/8.jpg)"></div>
                                    <span class="name ml-4">栗子</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="confirm-content" role="tabpanel">
                        <ul>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/9.jpg)"></div>
                                    <span class="name ml-4">米莎前輩</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/3.jpg)"></div>
                                    <span class="name ml-4">仁美</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/7.jpg)"></div>
                                    <span class="name ml-4">平手友梨奈</span>
                                </a>
                            </li>
                            <li>
                                <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                                    <div class="avatar rounded-circle" style="background-image:url(styles/images/confirmList/8.jpg)"></div>
                                    <span class="name ml-4">栗子</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>