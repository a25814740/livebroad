<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="createMap"">
            <button class="cancel"></button>
            <button class="next rounded-pill">下一步</button>
        </header>
        <!-- content -->
        <main class="mapGroup">
            <div id="map" class="map"></div>
            <div class="redLocation"></div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>