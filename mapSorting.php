<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="sorting">
            <div class="select black">
                <select name="amount" id="selectType">
                    <option value=" " selected> </option>
                    <option value="即時性">即時性</option>
                    <option value="社群">社群</option>
                    <option value="活動">活動</option>
                    <option value="社交">社交</option>
                    <option value="遊戲">遊戲</option>
                    <option value="專業">專業</option>
                </select>
            </div>
            <div class="header-right">
                <button type="button" class="no-bg-button sidebar-click-btn">
                    <span class="icon-menu f-38"></span>
                </button>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar mapSidebar">
            <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                <span class="icon-menu f-38"></span>
            </button>
            <div class="accordion" id="accordionExample">
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingOne">
                        <h5 class="mb-0">
                            <button class="mapBtn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                選擇分類
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <ul class="mapCardBody">
                            <li>
                                <a href=>
                                    <div class="icon fri"></div>
                                    <span class="ml-2">好友</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon per"></div>
                                    <span class="ml-2">追蹤</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon hot"></div>
                                    <span class="ml-2">熱門</span>
                                </a>
                            </li>
                            <li>
                                <a href=>
                                    <div class="icon loc"></div>
                                    <span class="ml-2">附近</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingTwo">
                        <h5 class="mb-0">
                            <a href="mapSorting.php" class="mapBtn collapsed d-block">
                                順序排列
                            </a>
                        </h5>
                    </div>
                </div>
                <div class="mapCard">
                    <div class="mapCardHeader" id="headingThree">
                        <h5 class="mb-0">
                            <a href="activityRecord.php" class="mapBtn collapsed d-block">
                                活動紀錄
                            </a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>

        
        <!-- <div class="sidebar dynamicSidebar">
            <div class="sidebar-header">
                <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                    <span class="icon-menu f-38"></span>
                </button>
            </div>
            <div class="sidebar-content">
                <ul>
                    <li>
                        <a href="#">
                            <div class="icon add"></div>
                            <span class="ml-3 f-18">發布動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon two"></div>
                            <span class="ml-3 f-18">全體動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon fri"></div>
                            <span class="ml-3 f-18">好友動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon per"></div>
                            <span class="ml-3 f-18">追蹤動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon hot"></div>
                            <span class="ml-3 f-18">熱門動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon loc"></div>
                            <span class="ml-3 f-18">附近動態</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div> -->
        <!-- content -->
        <main class="mapSortingGroup">
            <ul class="content" data-type="social">
                <li>
                    <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">個人</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">個人</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="othersInfo.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">個人</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="content" data-type="activity">
                <li>
                    <a href="activityContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">活動</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="activityContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">活動</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="activityContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">活動</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="content" data-type="now">
                <li>
                    <a href="nowContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">即時性</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="nowContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">即時性</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="nowContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">即時性</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="content" data-type="profession">
                <li>
                    <a href="professionContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">專業</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="professionContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">專業</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="professionContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">專業</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="content" data-type="community">
                <li>
                    <a href="communityIntroduction.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">社群</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="communityIntroduction.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">社群</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="communityIntroduction.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">社群</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="content" data-type="game">
                <li>
                    <a href="gameContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/2.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">遊戲</p>
                            <p class="time">2019/09/10 14:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="gameContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/4.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">遊戲</p>
                            <p class="time">2019/09/13 18:00</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="gameContent.php" class="row no-gutters justify-content-start align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/mapSorting/5.jpg)"></div>
                        <div class="col-5 ml-4">
                            <p class="sortTitle">遊戲</p>
                            <p class="time">2019/09/11 7:30</p>
                        </div>
                        <div class="money col-2 ml-4 d-flex justify-content-between align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        $('#selectType').on('change', function() {
            var thisType = $(this).val();
            console.log(thisType);
            switch (thisType) {
                case '社交':
                    $('.content').hide();
                    $('[data-type=social]').show();
                    break;
                case '活動':
                    $('.content').hide();
                    $('[data-type=activity]').show();
                    break;
                case '即時性':
                    $('.content').hide();
                    $('[data-type=now]').show();
                    break;
                case '專業':
                    $('.content').hide();
                    $('[data-type=profession]').show();
                    break;
                case '社群':
                    $('.content').hide();
                    $('[data-type=community]').show();
                    break;
                case '遊戲':
                    $('.content').hide();
                    $('[data-type=game]').show();
                    break;

                default:
                    break;
            }
        })
    });
</script>

</html>