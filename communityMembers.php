<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="deleteMember">
            <a href="communityContent.php" class="col-2"><span class="icon-back"></span></a>
            <div class="icon rounded-circle"></div>
            <h5 class="col-8">坂道遊戲群(190)</h5>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="deleteMemberGroup memberGroup">
            <div class="innerContent">
                <p class="title">成員</p>
                <div class="group">
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/1.jpg)"></div>
                            <span>麻友</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/2.jpg)"></div>
                            <span>咪醬</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/3.jpg)"></div>
                            <span>美金大</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/4.jpg)"></div>
                            <span>小楓</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/5.jpg)"></div>
                            <span>桃子</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/6.jpg)"></div>
                            <span>米莎前輩</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/7.jpg)"></div>
                            <span>仁美</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/8.jpg)"></div>
                            <span>平手友梨奈</span>
                        </a>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/communityMembers/9.jpg)"></div>
                            <span>栗子</span>
                        </a>
                        <div>
                        </div>
                    </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>