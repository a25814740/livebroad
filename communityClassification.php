<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="myCommunity.php" class="col-2">
                <span class="icon-back"></span>
            </a>
            <h3 class="col-8">社群分類</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="communityClassGroup">
            <form action="" class="innerContent h-100">
                <div class="editName ">
                    <label for="">分類名稱</label>
                    <input class="rounded-pill" type="text">
                </div>
                <ul class="group mt-3">
                    <li>
                        <label for="input1" class="news">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">LOL討論群(359)</span>
                            <input id="input1" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input2" class="news">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">芭樂咖蹦脆(6)</span>
                            <input id="input2" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input3">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">我們這一班315(37)</span>
                            <input id="input3" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input4">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">坂道遊戲群(190)</span>
                            <input id="input4" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input5">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">懷念京阿尼(542)</span>
                            <input id="input5" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input6" class="news">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">DEEMO(39)</span>
                            <input id="input6" type="checkbox">
                        </label>
                    </li>
                    <li>
                        <label for="input7" class="news">
                            <span class="new"></span>
                            <div class="img rounded-circle" style="background-image:url(styles/images/myCommunity/10.jpg)"></div>
                            <span class="title">耍廢交流(87)</span>
                            <input id="input7" type="checkbox">
                        </label>
                    </li>
                </ul>
                <div class="btnGroup d-flex justify-content-between align-items-center">
                    <input type="submit" value="確定" class="confirm rounded-pill">
                    <button type="button" class="cancel rounded-pill">取消</button>
                </div>
            </form>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>