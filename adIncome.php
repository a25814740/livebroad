<ul class="nav" id="adContent-tab" role="tablist">
    <li class="nav-item mr-2">
        <a class="nav-link" id="income-tab" data-toggle="tab" href="#income-content" role="tab">廣告收入明細</a>
    </li>
    <li class="nav-item">
        <a class="nav-link ml-2 active" id="dividend-tab" data-toggle="tab" href="#dividend-content" role="tab">每年紅利額度</a>
    </li>
</ul>
<div class="tab-content" id="adContent-content">
    <div class="tab-pane fade" id="income-content" role="tabpanel">
        <div class="pt-3">
            <?php include('userAdIncome.php') ?>
        </div>
    </div>
    <div class="tab-pane fade show active" id="dividend-content" role="tabpanel">
        <div class="dateGroup" id="dateGroup">
            <div class="row no-gutters title">
                <div class="col-3">日期</div>
                <div class="col-4">所占百分比</div>
                <div class="col-1"></div>
                <div class="col-2">收入</div>
                <div class="col-2"></div>
            </div>
            <div class="dateList">
                <div class="dateListHeader" id="dataHeading1">
                    <button class="row no-gutters w-100 align-items-center dateListBtn" type="button" data-toggle="collapse" data-target="#dateListCollapse1" aria-expanded="false">
                        <div class="date col-3">2016</div>
                        <div class="percentage col-4">0.005%</div>
                        <div class="moneyImg col-1"></div>
                        <div class="money col-2">43</div>
                        <div class="allow col-2"></div>
                    </button>
                </div>
                <div id="dateListCollapse1" class="collapse" data-parent="#dateGroup">
                    <div class="dateListBody">
                        <div class="item no-gutters">
                            <div class="twd rounded-pill col-4"><span class="smallText">新台幣2,000,000</span></div>
                            <div class="col-3"><span class="smallText">總數</span></div>
                            <div class="col-3"><span class="smallText">所占百分比</span></div>
                            <div class="col-2"><span class="smallText">收入</span></div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">受推薦活耀人數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">評價數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">發文數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>

                        
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">留言數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">分享數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">按讚數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">貼文瀏覽數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dateList">
                <div class="dateListHeader" id="dataHeading2">
                    <button class="row no-gutters w-100 align-items-center dateListBtn" type="button" data-toggle="collapse" data-target="#dateListCollapse2" aria-expanded="false">
                        <div class="date col-3">2017</div>
                        <div class="percentage col-4">0.002%</div>
                        <div class="moneyImg col-1"></div>
                        <div class="money col-2">47</div>
                        <div class="allow col-2"></div>
                    </button>
                </div>
                <div id="dateListCollapse2" class="collapse" data-parent="#dateGroup">
                    <div class="dateListBody">
                        <div class="item no-gutters">
                            <div class="twd rounded-pill col-4"><span class="smallText">新台幣2,000,000</span></div>
                            <div class="col-3"><span class="smallText">總數</span></div>
                            <div class="col-3"><span class="smallText">所占百分比</span></div>
                            <div class="col-2"><span class="smallText">收入</span></div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">受推薦活耀人數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">評價數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">發文數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>

                        
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">留言數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">分享數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">按讚數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">貼文瀏覽數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dateList">
                <div class="dateListHeader" id="dataHeading3">
                    <button class="row no-gutters w-100 align-items-center dateListBtn" type="button" data-toggle="collapse" data-target="#dateListCollapse3" aria-expanded="false">
                        <div class="date col-3">2018</div>
                        <div class="percentage col-4">0.003%</div>
                        <div class="moneyImg col-1"></div>
                        <div class="money col-2">23</div>
                        <div class="allow col-2"></div>
                    </button>
                </div>
                <div id="dateListCollapse3" class="collapse" data-parent="#dateGroup">
                    <div class="dateListBody">
                        <div class="item no-gutters">
                            <div class="twd rounded-pill col-4"><span class="smallText">新台幣2,000,000</span></div>
                            <div class="col-3"><span class="smallText">總數</span></div>
                            <div class="col-3"><span class="smallText">所占百分比</span></div>
                            <div class="col-2"><span class="smallText">收入</span></div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">受推薦活耀人數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">評價數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">發文數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>

                        
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">留言數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">分享數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">按讚數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">貼文瀏覽數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dateList">
                <div class="dateListHeader" id="dataHeading4">
                    <button class="row no-gutters w-100 align-items-center dateListBtn" type="button" data-toggle="collapse" data-target="#dateListCollapse4" aria-expanded="false">
                        <div class="date col-3">2019</div>
                        <div class="percentage col-4">0.004%</div>
                        <div class="moneyImg col-1"></div>
                        <div class="money col-2">30</div>
                        <div class="allow col-2"></div>
                    </button>
                </div>
                <div id="dateListCollapse4" class="collapse" data-parent="#dateGroup">
                    <div class="dateListBody">
                        <div class="item no-gutters">
                            <div class="twd rounded-pill col-4"><span class="smallText">新台幣2,000,000</span></div>
                            <div class="col-3"><span class="smallText">總數</span></div>
                            <div class="col-3"><span class="smallText">所占百分比</span></div>
                            <div class="col-2"><span class="smallText">收入</span></div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">受推薦活耀人數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">評價數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">發文數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>

                        
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">留言數</span></li>
                                <li><span class="smallText">總數40%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">354</div>
                            <div class="col-3">0.001%</div>
                            <div class="col-2">6</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">分享數</span></li>
                                <li><span class="smallText">總數30%</span></li>
                                <li><span class="smallText">600,000</span></li>
                            </ul>
                            <div class="col-3">100</div>
                            <div class="col-3">0.005%</div>
                            <div class="col-2">3</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">按讚數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                        <div class="item no-gutters">
                            <ul class="bigTitle col-4">
                                <li><span class="smallText">貼文瀏覽數</span></li>
                                <li><span class="smallText">總數10%</span></li>
                                <li><span class="smallText">800,000</span></li>
                            </ul>
                            <div class="col-3">40</div>
                            <div class="col-3">0.009%</div>
                            <div class="col-2">1</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>