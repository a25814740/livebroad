<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="map.php" class="col-2">
                <span class="icon-back"></span>
            </a>
            <h3 class="col-8">活動紀錄</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="createRecordGroup">
            <div class="innerHeader d-flex justify-content-end align-items-center">
                <div class="red"></div>
                <span>支付</span>
                <div class="blue"></div>
                <span>收取</span>
            </div>
            <ul class="content">
                <li>
                    <a href="giveContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/activityRecord/5.jpg)"></div>
                        <div class="col-5 ml-3">
                            <div class="info">
                                <span class="title">仁美</span>
                                <span class="account">@97205509</span>
                            </div>
                            <p class="time">2019-09-11 11:30</p>
                        </div>
                        <span class="type col-2">即時性</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="giveContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/activityRecord/3.jpg)"></div>
                        <div class="col-5 ml-3">
                            <div class="info">
                                <span class="title">桃子</span>
                                <span class="account">@momogo520</span>
                            </div>
                            <p class="time">2019-09-08 09:17</p>
                        </div>
                        <span class="type col-2">活動</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="blue"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="giveContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/activityRecord/4.jpg)"></div>
                        <div class="col-5 ml-3">
                            <div class="info">
                                <span class="title">咪醬</span>
                                <span class="account">@minamilove</span>
                            </div>
                            <p class="time">2019-09-07 23:28</p>
                        </div>
                        <span class="type col-2">遊戲</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="red"></div>
                            <span class="number">100</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="giveContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/activityRecord/7.jpg)"></div>
                        <div class="col-5 ml-3">
                            <div class="info">
                                <span class="title">小楓</span>
                                <span class="account">@saidokaede</span>
                            </div>
                            <p class="time">2019-09-05 07:10</p>
                        </div>
                        <span class="type col-2">專業</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="blue"></div>
                            <span class="number">100</span>
                        </div>
                    </a>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>