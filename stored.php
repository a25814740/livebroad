<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="myWallet flex-column">
            <div class="w-100 d-flex justify-content-between align-items-center">
                <a href="myWallet.php" class="col-2"><span class="icon-back"></span></a>
                <h3 class="col-8">儲值鑽石</h3>
                <div class="col-2"></div>
            </div>
            <div class="innerHeader w-100 d-flex justify-content-between align-items-center no-gutters">
                <a href="" class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/gift.svg)"></div>
                    <div class="info">
                        <p class="title">我的禮點</p>
                        <p>59347</p>
                    </div>
                </a>
                <a href="" class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/diamond.svg)"></div>
                    <div class="info">
                        <p class="title">我的鑽石</p>
                        <p>3394</p>
                    </div>
                </a>
                <a href="" class="item col-4">
                    <div class="icon" style="background-image:url(styles/images/myWallet/money.svg)"></div>
                    <div class="info">
                        <p class="title">我的現金</p>
                        <p>70346</p>
                    </div>
                </a>
            </div>
        </header>
        <!-- content -->
        <main class="storedGroup">
            <ul class="innerContent">
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">42鑽石</span>
                        </div>
                        <div class="moneyBtn">30元</div>
                    </button>
                </li>
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">210鑽石</span>
                        </div>
                        <div class="moneyBtn">150元</div>
                    </button>
                </li>
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">686鑽石</span>
                        </div>
                        <div class="moneyBtn">450元</div>
                    </button>
                </li>
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">2165鑽石</span>
                        </div>
                        <div class="moneyBtn">1420元</div>
                    </button>
                </li>
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">4116鑽石</span>
                        </div>
                        <div class="moneyBtn">2690元</div>
                    </button>
                </li>
                <li>
                    <button type="button" class="layui-btn h-auto text-dark" data-method="storeValue" data-type="auto">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="dim"></div>
                            <span class="money">11186鑽石</span>
                        </div>
                        <div class="moneyBtn">7190元</div>
                    </button>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>