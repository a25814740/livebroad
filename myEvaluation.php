<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="evaluation.php" class="col-2">
                <span class="icon-back"></span>
            </a>
            <h3 class="col-8">我的評價</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="myEvaluationGroup">
            <div class="section row no-gutters">
                <div class="avatar rounded-circle" style="background-image:url(styles/images/common/guest.svg)"></div>
                <ul class="info col-9">
                    <div class="type">信用度:4星</div>
                    <div class="star">
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img no"></div>
                    </div>
                </ul>
            </div>
            <div class="section row no-gutters">
                <div class="avatar rounded-circle" style="background-image:url(styles/images/myEvaluation/3.jpg)"></div>
                <ul class="info col-9">
                    <ul class="person">
                        <li>
                            <span class="name">桃子</span>
                            <span class="account">@momoko</span>
                        </li>
                        <li>
                            <span class="area">桃園市，大園區</span>
                            <span class="time">2019/09/14</span>
                        </li>
                    </ul>
                    <div class="type">專業能力:5星</div>
                    <div class="star">
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                    </div>
                    <p class="text col-10">
                        我今天跟他玩了遊戲,超級carry的陪我爬上了鑽石!真是太棒了!
                    </p>
                    <div class="content">
                        <!-- <div class="img" style="background-image:url(styles/images/myEvaluation/9.jpg)"></div> -->
                        <!-- <div class="img" style="background-image:url(styles/images/myEvaluation/8.jpg)"></div> -->
                        <a class="img" href="styles/images/myEvaluation/9.jpg" data-lightbox="styles/images/myEvaluation/9.jpg">
                            <img src="styles/images/myEvaluation/9.jpg" alt="">
                        </a>
                        <a class="img" href="styles/images/myEvaluation/8.jpg" data-lightbox="styles/images/myEvaluation/8.jpg">
                            <img src="styles/images/myEvaluation/8.jpg" alt="">
                        </a>
                    </div>
                </ul>
            </div>
            <div class="section row no-gutters">
                <div class="avatar rounded-circle" style="background-image:url(styles/images/common/guest.svg)"></div>
                <ul class="info col-9">
                    <div class="type">行為:1星</div>
                    <div class="star">
                        <div class="img yes"></div>
                        <div class="img no"></div>
                        <div class="img no"></div>
                        <div class="img no"></div>
                        <div class="img no"></div>
                    </div>
                </ul>
            </div>
            <div class="section row no-gutters">
                <div class="avatar rounded-circle" style="background-image:url(styles/images/myEvaluation/5.jpg)"></div>
                <ul class="info col-9">
                    <ul class="person">
                        <li>
                            <span class="name">ten醬</span>
                            <span class="account">@kaede_46</span>
                        </li>
                        <li>
                            <span class="area">高雄市，小港區</span>
                            <span class="time">2019/08/26</span>
                        </li>
                    </ul>
                    <div class="type">身份識別:5星</div>
                    <div class="star">
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                        <div class="img yes"></div>
                    </div>
                    <p class="text col-10">
                        面交速度快,態度優良<br>
                        貨物的品質也很棒
                    </p>
                </ul>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>