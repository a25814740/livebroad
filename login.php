<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="login w-100 h-100">
            <div class="innerLogin d-flex flex-column justify-content-center align-items-center">
                <div class="innerHeader"></div>
                <form action="personInfo.php" class="innerContent d-flex flex-column justify-content-center align-items-center text-white">
                    <div class="input d-flex justify-content-center align-items-center">
                        <div class="icon icon-person f-20"></div>
                        <input type="text" placeholder="帳號信息">
                    </div>
                    <div class="input mt-4 d-flex justify-content-center align-items-center">
                        <div class="icon icon-lock1 f-20"></div>
                        <input type="password" placeholder="密碼">
                    </div>
                    <input type="submit" class="logBtn mt-4 rounded" value="登入">
                    <a href="" class="forBtn">忘記帳號?</a>
                    <a href="register.php" class="regBtn mt-4 rounded text-center">註冊帳號</a>
                    <button type="button" class="regBtn mt-4 rounded border-0">
                        <img src="styles/images/login/4.png" alt="">
                    </button>
                </form>
            </div>
        </main>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>