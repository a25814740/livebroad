<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="create.php" class="col-2">
                <span class="icon-back"></span>
            </a>
            <h3 class="col-7">創建紀錄</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="createRecordGroup">
            <div class="innerHeader d-flex justify-content-end align-items-center">
                <div class="red"></div>
                <span>支付</span>
                <div class="blue"></div>
                <span>收取</span>
            </div>
            <ul class="content">
                <li>
                    <a href="recordContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="col-7">
                            <p class="title">求外送</p>
                            <p class="time">2019-09-11 11:30</p>
                        </div>
                        <span class="type col-3">即時性</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="red"></div>
                            <span class="number">10</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="recordContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="col-7">
                            <p class="title">假日市集</p>
                            <p class="time">2019-09-08 09:17</p>
                        </div>
                        <span class="type col-3">活動</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="blue"></div>
                            <span class="number">50</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="recordContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="col-7">
                            <p class="title">奇蹟暖暖</p>
                            <p class="time">2019-09-07 23:28</p>
                        </div>
                        <span class="type col-3">遊戲</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="red"></div>
                            <span class="number">100</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="recordContent.php" class="row no-gutters justify-content-between align-items-center">
                        <div class="col-7">
                            <p class="title">修水管</p>
                            <p class="time">2019-09-05 7:10</p>
                        </div>
                        <span class="type col-3">專業</span>
                        <div class="money col-2 d-flex justify-content-center align-items-center">
                            <div class="blue"></div>
                            <span class="number">100</span>
                        </div>
                    </a>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>