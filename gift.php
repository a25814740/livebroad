<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="javascript:window.history.back();" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">贈禮選擇</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="giftGroup">
            <div class="innerHeader d-flex justify-content-end align-items-center">
                <span>目前鑽石:</span>
                <span>723個</span>
            </div>
            <div class="group">
                <div class="group-wrapper row no-gutters">
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-1.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">10</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-2.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">30</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-3.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">50</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-4.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">85</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-5.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">100</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-6.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">120</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-7.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">145</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-8.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">165</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-9.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">180</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-7.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">145</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-8.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">165</span>
                        </div>
                    </div>
                    <div class="col-4 gift">
                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-9.svg)"></div>
                        <div class="info">
                            <div class="blue"></div>
                            <span class="number">180</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerFooter">
                <button type="button" class="giftBtn rounded-pill">確定送出</button>
            </div>
            <!-- <button type="button" class="giftBtn rounded-pill">確定送出</button> -->
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    $(document).ready(function() {
        $('.gift').on('click', function() {
            $('.gift').removeClass('active');
            $(this).addClass('active');
        })
    });
</script>

</html>