<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>給予評價</h3>
        </header>
        <!-- content -->
        <main class="giveEvaluationGroup">
            <form action="" class="innerContainer">
                <div class="no-gutters black d-flex justify-content-start align-items-center">
                    <label class="mb-0" for="">選擇類別：</label>
                    <div class="select ml-3">
                        <select name="evaluation" id="">
                            <option value="身份識別" selected>身份識別</option>
                            <option value="信用">信用</option>
                            <option value="行為">行為</option>
                            <option value="專業能力">專業能力</option>
                            <option value="服務滿意">服務滿意</option>
                        </select>
                    </div>
                </div>
                <div class="star d-flex justify-content-center align-items-center">
                    <div class="img yes"></div>
                    <div class="img yes"></div>
                    <div class="img yes"></div>
                    <div class="img yes"></div>
                    <div class="img no"></div>
                </div>
                <!-- <div class="star d-flex justify-content-center align-items-center">
                    <button type="button"></button>
                    <button type="button"></button>
                    <button type="button"></button>
                    <button type="button"></button>
                    <button type="button"></button>
                </div> -->
                <div class="no-gutters black d-flex flex-column justify-content-center align-items-start mb-4">
                    <label for="" class="mb-1">文字敘述：</label>
                    <textarea name="" id="" cols="30" rows="10" class="text-white px-3 py-2"></textarea>
                </div>
                <div class="uploadImgGroup d-flex flex-column justify-content-center align-items-start mb-2">
                    <label for="">上傳圖片：</label>
                    <input type="file" class="w-100">
                    <div class="uploadImg">
                        <div class="imgLabel"></div>
                    </div>
                </div>
                <div class="btnGroup d-flex justify-content-between align-items-center w-100 mt-5">
                    <input type="submit" value="確定評價" class="confirm rounded-pill">
                    <button type="button" class="cancel rounded-pill">取消評價</button>
                </div>
            </form>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>