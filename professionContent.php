<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="recordContent map">
            <div class="col-2"></div>
            <div class="icon rounded-circle"></div>
            <h3 class="col-8">徵求日文家教</h3>
            <div class="avatar icon rounded-circle"></div>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="recordContentGroup">
            <ul class="innerContent">
                <li class="typeGroup mb-2">
                    <span class="title">類別:</span>
                    <span class="type">活動</span>
                </li>
                <li class="banner" style="background-image:url(styles/images/activityContent/10.jpg)"></li>
                <li class="contentGroup mt-2">
                    <p class="title">詳細內容:</p>
                    <div class="text">想要推展的就是同小蝸牛一般的生活態度，市集邀集來的品牌要有共同的理念，不會是以商業、速度取勝的，而是堅持著自己的想法，慢慢的經營自己專注的事物。</div>
                </li>
                <li class="timeGroup mt-2">
                    <span class="title">支付時間:</span>
                    <span class="date">2019/09/08</span>
                    <span class="time">09:17</span>
                </li>
                <li class="moneyGroup mt-2">
                    <span class="title">支付額度:</span>
                    <span class="money">0鑽</span>
                    <div class="red"></div>
                </li>
                <li class="peopleGroup">
                    <a href="confirmContent.php">
                        <div class="app">
                            <p class="number">已申請人數:4</p>
                            <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/1.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/2.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/3.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/4.jpg)"></div>
                            </div>
                        </div>
                        <div class="add">
                            <p class="number">已確認人數:2</p>
                            <div class="avatarGroup mt-2 d-flex justify-content-center align-items-center">
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/5.jpg)"></div>
                                <div class="avatar rounded-circle" style="background-image:url(styles/images/activityContent/6.jpg)"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <div class="btnGroup d-flex justify-content-between align-items-center">
                    <input type="submit" value="確定" class="confirm rounded-pill">
                    <a href="map.php" class="cancel rounded-pill">取消</a>
                </div>
            </ul>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>