<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="createSettingGroup">
            <form action="" class="innerContent h-100 d-flex flex-column justify-content-between">
                <div class="top">
                    <img src="styles/images/videoContent/4.jpg" class="img-fluid" alt="Responsive image">
                    <ul class="mt-5">
                        <li>
                            <label for="">設定額度:</label>
                            <div class="select black ml-3">
                                <select name="amount" id="">
                                    <option value="10鑽" selected>10鑽</option>
                                    <option value="20鑽">20鑽</option>
                                    <option value="30鑽">30鑽</option>
                                    <option value="40鑽">40鑽</option>
                                    <option value="50鑽">50鑽</option>
                                    <option value="60鑽">60鑽</option>
                                    <option value="70鑽">70鑽</option>
                                    <option value="80鑽">80鑽</option>
                                    <option value="90鑽">90鑽</option>
                                    <option value="100鑽">100鑽</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="btnGroup d-flex justify-content-end align-items-center mt-3 w-100">
                    <button type="submit" class="btn border-0 p-0 f-32"><span class="icon-sent-massage" style="color: #FFD84F;"></span></button>
                </div>
            </form>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>