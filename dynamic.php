<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="dynamic">
            <div class="header-right">
                <button type="button" class="no-bg-button sidebar-click-btn">
                    <span class="icon-menu f-38"></span>
                </button>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar dynamicSidebar">
            <div class="sidebar-header">
                <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                    <span class="icon-menu f-38"></span>
                </button>
                <div class="header-user d-flex align-items-end">
                    <div class="icon rounded-circle"></div>
                    <div class="username">@nanase0525</div>
                </div>
            </div>
            <div class="sidebar-content">
                <ul>
                    <li>
                        <a href="#">
                            <div class="icon add"></div>
                            <span class="ml-3 f-18">發布動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon two"></div>
                            <span class="ml-3 f-18">全體動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon fri"></div>
                            <span class="ml-3 f-18">好友動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon per"></div>
                            <span class="ml-3 f-18">追蹤動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon hot"></div>
                            <span class="ml-3 f-18">熱門動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon loc"></div>
                            <span class="ml-3 f-18">附近動態</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- content -->
        <main class="dynamicGroup">
            <!-- 自己動態 -->
            <div class="section" data-type="own">
                <div class="innerHeader d-flex justify-content-between align-items-center">
                    <a href="othersInfo.php">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/dynamic/5.jpg)"></div>
                        <span class="account">manatsu0820</span>
                    </a>
                    <button type="button" class="more"></button>
                </div>
                <div class="innerContent">
                    <!-- 如果解鎖 直接移除Class => blur -->
                    <div class="swiper-container blur-wrapper">
                        <div class="swiper-wrapper">
                            <div class="image swiper-slide blur-item layui-btn" data-method="unlock" data-type="auto" style="background-image:url(styles/images/dynamic/9.jpg)"></div>
                            <div class="image swiper-slide blur-item layui-btn" data-method="unlock" data-type="auto" style="background-image:url(styles/images/dynamic/8.jpg)"></div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <div class="innerContainer">
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="number">
                                <a href="javascript:;" class="thumb">5394個讚</a>
                                <span class="time">7個小時前</span>
                            </div>
                            <div class="icon d-flex justify-content-between align-items-center">
                                <button type="button" class="icon-heart"></button>
                                <button type="button" class="icon-gift"></button>
                                <button type="button" class="icon-share"></button>
                                <button type="button" class="icon-collection"></button>
                            </div>
                        </div>
                        <div class="innerText">
                            <p class="text">今天上zip節目大家幫我慶生</p>
                            <a href="javascript:;" class="tag">#秋元真夏</a>
                            <a href="javascript:;" class="tag">#秋元真夏生誕祭</a>
                        </div>
                        <button type="button" class="others">其他678則留言</button>
                        <form action="" class="message-wrapper mt-1">
                            <input type="text" class="respond rounded-pill w-100 f-12" placeholder="留言回應....">
                            <button type="submit" class="sent-btn">
                                <span class="icon-sent-massage"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- 他人動態 -->
            <div class="section" data-type="other">
                <div class="innerHeader d-flex justify-content-between align-items-center">
                    <a href="othersInfo.php">
                        <div class="avatar rounded-circle" style="background-image:url(styles/images/dynamic/7.jpg)"></div>
                        <span class="account">manatsu0820</span>
                    </a>
                    <button type="button" class="more"></button>
                </div>
                <div class="innerContent">
                    <div class="swiper-container blur-wrapper">
                        <div class="swiper-wrapper">
                            <div class="image swiper-slide blur-item layui-btn" data-method="unlock" data-type="auto" style="background-image:url(styles/images/dynamic/4.jpg)"></div>
                            <div class="image swiper-slide blur-item layui-btn" data-method="unlock" data-type="auto" style="background-image:url(styles/images/dynamic/3.jpg)"></div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <div class="innerContainer">
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="number">
                                <a href="javascript:;" class="thumb">5394個讚</a>
                                <span class="time">7個小時前</span>
                            </div>
                            <div class="icon d-flex justify-content-between align-items-center">
                                <button type="button" class="icon-heart"></button>
                                <button type="button" class="icon-gift"></button>
                                <button type="button" class="icon-share"></button>
                                <button type="button" class="icon-collection"></button>
                            </div>
                        </div>
                        <div class="innerText">
                            <p class="text">今天上zip節目大家幫我慶生</p>
                            <a href="javascript:;" class="tag">#秋元真夏</a>
                            <a href="javascript:;" class="tag">#秋元真夏生誕祭</a>
                        </div>
                        <button type="button" class="others">其他678則留言</button>
                        <form action="" class="message-wrapper mt-1">
                            <input type="text" class="respond rounded-pill w-100 f-12" placeholder="留言回應....">
                            <button type="submit" class="sent-btn">
                                <span class="icon-sent-massage"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <div id="own" class="layer-own">
            <ul>
                <li><a href="#">複製連結</a></li>
                <li><a href="#">編輯貼文</a></li>
                <li><a href="#">刪除貼文</a></li>
            </ul>
        </div>
        <div id="other" class="layer-other">
            <ul>
                <li><a href="#">複製連結</a></li>
                <li><a href="#">封鎖</a></li>
                <li><a href="#">停止追蹤</a></li>
            </ul>
        </div>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
    });
    $(document).ready(function() {
        $('.more').on('click', function() {
            var thisType = $(this).parents('.section').attr('data-type');
            switch (thisType) {
                case 'own':
                    layer.open({
                        type: 1,
                        title: false,
                        skin: 'layui-layer-dynamic',
                        closeBtn: 0,
                        anim: 2,
                        shadeClose: true,
                        content: $('#own')
                    });
                    break;
                case 'other':
                    layer.open({
                        type: 1,
                        title: false,
                        skin: 'layui-layer-dynamic',
                        closeBtn: 0,
                        anim: 2,
                        shadeClose: true,
                        content: $('#other')
                    });
                    break;

            }
        })
    });
</script>

</html>