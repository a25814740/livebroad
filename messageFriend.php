<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>訊息</h3>
        </header>
        <!-- content -->
        <main class="friendGroup">
            <div class="innerHeader">
                <form action="">
                    <div class="custom-search">
                        <input type="text" class="custom-search-input" required>
                        <input type="submit" value="">
                        <span class="icon-search"></span>
                    </div>
                </form>
            </div>
            <div class="innerContainer">
                <ul class="nav justify-content-around align-items-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="friend-tab" data-toggle="pill" href="#friend-content" role="tab">好友</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="stranger-tab" data-toggle="pill" href="#stranger-content" role="tab">陌生人</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="friend-content" role="tabpanel">
                        <div class="messageFriendGroup inviteMemberGroup memberGroup">
                            <div class="innerContent h-auto p-0">
                                <div class="group">
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/1.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input1" class="m-0">麻友</label>
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎? 今天有空一起吃飯嗎? 今天有空一起吃飯嗎? 今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle">25</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/2.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input2" class="m-0">咪醬</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">好的</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle">11</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/3.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input3" class="m-0">美金大</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">傳送了禮物</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle">20</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/4.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input4" class="m-0">小楓</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">看情況 ...</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle">9</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/5.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input5" class="m-0">桃子</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle">9</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/6.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input6" class="m-0">米莎前輩</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle d-none">9</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/7.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input7" class="m-0">仁美</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle d-none">9</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/8.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input8" class="m-0">平手友梨奈</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle d-none">9</span>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/9.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input9" class="m-0">栗子</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <span class="badge badge-light rounded-circle d-none">9</span>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="stranger-content" role="tabpanel">
                    <div class="messageFriendGroup inviteMemberGroup memberGroup">
                            <div class="innerContent h-auto p-0">
                                <div class="group">
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/1.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input1" class="m-0">麻友</label>
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">新小說看了嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/2.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input2" class="m-0">咪醬</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天在韓國拍攝</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle">25</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/3.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input3" class="m-0">美金大</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">最近拍電影比較累</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/4.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input4" class="m-0">小楓</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">看情況 ...</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/5.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input5" class="m-0">桃子</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/6.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input6" class="m-0">米莎前輩</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/7.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input7" class="m-0">仁美</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">9</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/8.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input8" class="m-0">平手友梨奈</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle d-none">3</span>
                                        </div>
                                    </li>
                                    <li class="row no-gutters info">
                                        <div class="avatar rounded-circle col-auto" style="background-image:url(styles/images/friendInvite/9.jpg)"></div>
                                        <div class="info-group col ml-4">
                                            <label for="input9" class="m-0">栗子</label>
                                        
                                            <div class="edit dotdotdot-line-1">
                                                <p class="f-12">今天有空一起吃飯嗎?</p>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <span class="badge badge-light rounded-circle">9</span>
                                        </div>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>