<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="othersInfo">
            <div class="content">
                <div class="banner" style="background-image: url('styles/images/others_info/banner.png')"></div>
                <ul class="info">
                    <li class="top row no-gutters">
                        <div class="photo col">
                            <div class="photo-item" style="background-image: url(styles/images/others_info/photo.png)"></div>
                        </div>
                        <div class="recommend rounded-pill">已推薦</div>
                        <ul class="quantity col-6 row no-gutters f-20">
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">368K</span>
                                <span class="f-12">讚數</span>
                            </li>
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">46K</span>
                                <span class="f-12">追蹤</span>
                            </li>
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">399</span>
                                <span class="f-12">好友</span>
                            </li>
                        </ul>
                    </li>
                    <li class="mid row no-gutters align-items-stretch">
                        <div class="col-6 f-12">
                            <span class="d-inline-block">NANASE</span>
                            <span class="d-inline-block ml-1">@05251992</span>
                        </div>
                        <div class="col-6 d-flex mt-2">
                            <!-- .img 加 active 即可換色 -->
                            <div class="col-3 px-2">
                                <a href="" class="d-block img active"><span class="d-block icon-msg-w"></span></a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="" class="d-block img"><span class="d-block icon-add-w"></span></a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="" class="d-block img"><span class="d-block icon-check-w"></span></a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="" class="d-block img"><span class="d-block icon-share-w"></span></a>
                            </div>
                        </div>
                        <div class="editor col-7 -14f">
                            <div class="edit dotdotdot-line-3">
                                <p>
                                    Graphic designer / Photoshop / Illustrator / 2D generalist / EN/ESP / contact: camilacarbia@gmail.com
                                </p>
                            </div>
                        </div>
                        <div class="col"></div>
                    </li>
                </ul>
            </div>
            <div class="menu">
                <ul class="nav nav-pills row no-gutters button list-unstyled" id="pills-tab" role="tablist">
                    <li class="nav-item col-3 text-center act">
                        <a href="#tab-content-1" class="nav-link active" id="tab-1" data-toggle="pill">
                            動態
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-2" class="nav-link" id="tab-2" data-toggle="pill">
                            關於我
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-3" class="nav-link" id="tab-3" data-toggle="pill">
                            品味商鋪
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-4" class="nav-link" id="tab-4" data-toggle="pill">
                            評價
                        </a>
                    </li>
                </ul>
                <div class="group tab-content justify-content-center align-items-center" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="tab-content-1">
                        <div class="row no-gutters w-100 features">
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/1.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/2.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/3.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/4.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/5.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/6.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/7.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/8.jpg)"></div>
                                </a>
                            </li>
                            <li class="col-4">
                                <a href="#" class="d-block">
                                    <div class="img" style="background-image:url(styles/images/others_info/9.jpg)"></div>
                                </a>
                            </li>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-content-2">
                        <div class="row no-gutters">
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>性別</span>
                                <div class="sex d-flex">
                                    <div class="form-check form-check-inline custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="inlineRadio2" name="radio-stacked" required disabled>
                                        <label class="custom-control-label" for="inlineRadio2">男性</label>
                                    </div>
                                    <div class="form-check form-check-inline custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="inlineRadio1" name="radio-stacked" checked required>
                                        <label class="custom-control-label" for="inlineRadio1">女性</label>
                                    </div>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>生日</span>
                                <div class="birthday d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="1994">
                                    <span>年</span>
                                    <input type="text" readonly value="5">
                                    <span>月</span>
                                    <input type="text" readonly value="25">
                                    <span>日</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>身高</span>
                                <div class="height d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="176">
                                    <span>公分</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>體重</span>
                                <div class="weight d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="未公開">
                                    <span>公斤</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>電話</span>
                                <div class="tel d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="0946875915">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>信箱</span>
                                <div class="mail d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="g2584987@gmail.com">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>語言</span>
                                <div class="language d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="中文">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>學歷</span>
                                <div class="education d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="新竹教育大學">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>感情狀況</span>
                                <div class="career d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="單身">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>職業</span>
                                <div class="career d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="學生">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>專業</span>
                                <div class="profession d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="設計">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show row no-gutters" id="tab-content-3">

                    </div>
                    <div class="tab-pane fade show row no-gutters" id="tab-content-4">
                        <div class="evaluation">
                            <ul class="nav nav-tabs justify-content-between align-items-center" id="nav-tab" role="tablist">
                                <li class="nav-item">
                                    <a href="#evaluation-content1" class="nav-link active" id="evaluation-tab1" data-toggle="pill">個人評價</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#evaluation-content2" class="nav-link" id="evaluation-tab2" data-toggle="pill">活動評價</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="evaluation-content1">
                                    <ul>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">身分識別:4星</p>
                                                <span class="number">目前9次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">信用:2星</p>
                                                <span class="number">目前4次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">行為:3星</p>
                                                <span class="number">目前6次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">專業能力:4星</p>
                                                <span class="number">目前17次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li class="d-flex justify-content-center align-items-center pb-3">
                                            <a href="giveEvaluation.php" class="rounded-pill">給予評價</a>
                                        </li>
                                    </ul>

                                </div>
                                <div class="tab-pane fade" id="evaluation-content2">
                                    <ul>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">身分識別:4星</p>
                                                <span class="number">目前9次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">信用:2星</p>
                                                <span class="number">目前4次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">行為:3星</p>
                                                <span class="number">目前6次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">專業能力:4星</p>
                                                <span class="number">目前17次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="info d-flex justify-content-between align-items-center">
                                                <p class="type">服務滿意:4星</p>
                                                <span class="number">目前26次評價</span>
                                            </div>
                                            <div class="star d-flex justify-content-between align-items-center">
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img yes"></div>
                                                <div class="img no"></div>
                                            </div>
                                        </li>
                                        <li class="d-flex justify-content-center align-items-center pb-3">
                                            <a href="giveEvaluation.php" class="rounded-pill">給予評價</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>