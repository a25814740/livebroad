<!-- 公開設定開關 -->
<label class="setBtn">
    <input type="checkbox" checked>
    <span class="slider"></span>
</label>
<!-- 搜尋框 -->
<div class="custom-search">
    <input type="text" class="custom-search-input" required>
    <input type="submit" value="">
    <span class="icon-search"></span>
</div>
<!-- 選擇框 -->
<div class="select black ml-3">
    <select name="amount" id="">
        <option value="" selected></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
        <option value=""></option>
    </select>
</div>
<!-- 按鈕組 -->
<div class="btnGroup d-flex justify-content-between align-items-center">
    <input type="submit" value="確定創建" class="confirm rounded-pill">
    <button type="button" class="cancel rounded-pill">取消創建</button>
</div>
<!-- 上傳圖片組 -->
<div class="uploadImgGroup row no-gutters justify-content-start align-items-center">
    <label for="">上傳圖片：</label>
    <input type="file" class="col-12">
    <div class="uploadImg">
        <div class="imgLabel"></div>
    </div>
</div>