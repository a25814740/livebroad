<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- content -->
        <main class="personInfo">
            <div class="content">
                <div class="banner" style="background-image: url('styles/images/person_info/banner.png')"></div>
                <ul class="info">
                    <li class="top row no-gutters">
                        <div class="photo col">
                            <div class="photo-item" style="background-image: url(styles/images/person_info/photo.png)"></div>
                        </div>
                        <div class="gift col-auto d-flex flex-column justify-content-center align-items-end f-12">
                            <div class="gift-item">
                                <img src="styles/icons/person_info/info1.png" alt="">
                                <span class="ml-1">4346</span>
                            </div>
                            <div class="gift-item">
                                <img src="styles/icons/person_info/info2.png" alt="">
                                <span class="ml-1">316K</span>
                            </div>
                        </div>
                        <ul class="quantity col-6 row no-gutters f-20">
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">368K</span>
                                <span class="f-12">讚數</span>
                            </li>
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">46K</span>
                                <span class="f-12">追蹤</span>
                            </li>
                            <li class="col-4 d-flex flex-column justify-content-center align-items-center">
                                <span class="number">399</span>
                                <span class="f-12">好友</span>
                            </li>
                        </ul>
                    </li>
                    <li class="mid row no-gutters align-items-stretch">
                        <div class="col-6 f-12">
                            <span class="d-inline-block">NANASE</span>
                            <span class="d-inline-block ml-1">@05251992</span>
                        </div>
                        <div class="col-6 d-flex">
                            <div class="col-3 px-2">
                                <a href="searchFriend.php" class="d-block">
                                    <img src="styles/icons/person_info/tool1.png" alt="">
                                </a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="blacklist.php" class="d-block">
                                    <img src="styles/icons/person_info/tool2.png" alt="">
                                </a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="personEdit.php" class="d-block">
                                    <img src="styles/icons/person_info/tool3.png" alt="">
                                </a>
                            </div>
                            <div class="col-3 px-2">
                                <a href="" class="d-block">
                                    <img src="styles/icons/person_info/tool4.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="editor col-7 -14f">
                            <div class="edit dotdotdot-line-3">
                                <p>
                                    Graphic designer / Photoshop / Illustrator / 2D generalist / EN/ESP / contact: camilacarbia@gmail.com
                                </p>
                            </div>
                        </div>
                        <div class="col"></div>
                    </li>
                </ul>
            </div>
            <div class="menu">
                <ul class="nav nav-pills row no-gutters button list-unstyled" id="pills-tab" role="tablist">
                    <li class="nav-item col-3 text-center act">
                        <a href="#tab-content-1" class="nav-link active" id="tab-1" data-toggle="pill">
                            功能
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-2" class="nav-link" id="tab-2" data-toggle="pill">
                            關於我
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-3" class="nav-link" id="tab-3" data-toggle="pill">
                            通知
                        </a>
                    </li>
                    <li class="nav-item col-3 text-center">
                        <a href="#tab-content-4" class="nav-link" id="tab-4" data-toggle="pill">
                            品味商鋪
                        </a>
                    </li>
                </ul>
                <div class="group tab-content justify-content-center align-items-center" id="pills-tabContent">
                    <div class="tab-pane fade show active row no-gutters features" id="tab-content-1">
                        <div class="row w-100 h-100 m-0">
                            <a href="#" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu1.png" alt="" srcset="">
                                    <span>訊息</span>
                                </span>
                            </a>
                            <a href="friend.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu2.png" alt="" srcset="">
                                    <span>好友</span>
                                </span>
                            </a>
                            <a href="map.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu3.png" alt="" srcset="">
                                    <span>我的附近</span>
                                </span>
                            </a>
                            <a href="community.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu4.png" alt="" srcset="">
                                    <span>社群</span>
                                </span>
                            </a>
                            <a href="#" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu5.png" alt="" srcset="">
                                    <span>動態訊息</span>
                                </span>
                            </a>
                            <a href="mediaIndex.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu6.png" alt="" srcset="">
                                    <span>媒體影音</span>
                                </span>
                            </a>
                            <a href="evaluation.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu7.png" alt="" srcset="">
                                    <span>我的評價</span>
                                </span>
                            </a>
                            <a href="create.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu8.png" alt="" srcset="">
                                    <span>創建</span>
                                </span>
                            </a>
                            <a href="myWallet.php" class="col-4">
                                <span class="menu-link-group d-block">
                                    <img src="styles/icons/person_info/menu9.png" alt="" srcset="">
                                    <span>錢包</span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-content-2">
                        <div class="row no-gutters">
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>性別</span>
                                <div class="sex d-flex">
                                    <div class="form-check form-check-inline custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="inlineRadio2" name="radio-stacked" required disabled>
                                        <label class="custom-control-label" for="inlineRadio2">男性</label>
                                    </div>
                                    <div class="form-check form-check-inline custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="inlineRadio1" name="radio-stacked" checked required>
                                        <label class="custom-control-label" for="inlineRadio1">女性</label>
                                    </div>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>生日</span>
                                <div class="birthday d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="1994">
                                    <span>年</span>
                                    <input type="text" readonly value="5">
                                    <span>月</span>
                                    <input type="text" readonly value="25">
                                    <span>日</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>身高</span>
                                <div class="height d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="176">
                                    <span>公分</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>體重</span>
                                <div class="weight d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="53">
                                    <span>公斤</span>
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>電話</span>
                                <div class="tel d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="0946875915">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>信箱</span>
                                <div class="mail d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="g2584987@gmail.com">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>語言</span>
                                <div class="language d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="中文">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>學歷</span>
                                <div class="education d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="新竹教育大學">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>感情狀況</span>
                                <div class="career d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="單身">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>職業</span>
                                <div class="career d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="學生">
                                </div>
                            </div>
                            <div class="read d-flex justify-content-between align-items-center">
                                <span>專業</span>
                                <div class="profession d-flex justify-content-start align-items-center">
                                    <input type="text" readonly value="設計">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show row no-gutters" id="tab-content-3">
                        <div class="news w-100">
                            <ul class="today">
                                <div class="title">今天</div>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/1.jpg)"></div>
                                    <div class="text">
                                        <a href="javascript:;">sunbaby71</a>、<a href="javascript:;">kmnrkmvh</a>和<a href="javascript:;">其他3人</a>都說你的貼文讚。<span>2小時前</span>
                                    </div>
                                    <div class="type icon-favorite"></div>
                                </li>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/2.jpg)"></div>
                                    <div class="text">
                                        <a href="javascript:;">sweet.21</a>、<a href="javascript:;">kaoooo</a>開始追蹤你。<span>6小時前</span>
                                    </div>
                                    <div class="type icon-member"></div>
                                </li>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/4.jpg)"></div>
                                    <div class="text">
                                        <a href="javascript:;">iamsien</a>給了你評價。<span>18小時前</span>
                                    </div>
                                    <div class="type icon-crown"></div>
                                </li>
                            </ul>
                            <ul class="week">
                                <div class="title">本週</div>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/5.jpg)"></div>
                                    <div class="text">
                                        <a>jonathan8611</a>說你的貼文讚。<span>2天前</span>
                                    </div>
                                    <div class="type icon-favorite"></div>
                                </li>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/2.jpg)"></div>
                                    <div class="text">
                                        <a href="javascript:;">sweet.21</a>、<a href="javascript:;">kaoooo</a>開始追蹤你。<span>6小時前</span>
                                    </div>
                                    <div class="type icon-member"></div>
                                </li>
                                <li>
                                    <div class="img rounded-circle" style="background-image:url(styles/images/person_info/4.jpg)"></div>
                                    <div class="text">
                                        <a href="javascript:;">iamsien</a>給了你評價。<span>18小時前</span>
                                    </div>
                                    <div class="type icon-crown"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade show row no-gutters" id="tab-content-4"></div>
                </div>
            </div>
        </main>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>