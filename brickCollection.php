<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header style="padding: .25rem 1rem .75rem 1rem;">
            <div class="groupHeader d-flex justify-content-between align-items-center w-100">
                <div class="header-user d-flex align-items-end">
                    <div class="icon rounded-circle"></div>
                    <div class="username">@nanase0525</div>
                </div>
                <div class="header-right">
                    <button type="button" class="no-bg-button sidebar-click-btn">
                        <span class="icon-menu f-38"></span>
                    </button>
                </div>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar" style="padding-top: 2rem;">
            <div class="sidebar-header">
                <button type="button" class="close-button icons sidebar-click-btn header-right" style="top: .25rem;">
                    <span class="icon-menu"></span>
                </button>
                <a href="brickGeneral.php" class="header-user d-flex align-items-end">
                    <div class="icon rounded-circle"></div>
                    <div class="username">@nanase0525</div>
                </a>
            </div>
            <div class="sidebar-content">
                <ul>
                    <li>
                        <a href="uploadDynamic.php" class="d-flex align-items-center">
                            <span class="icon-menu-1 f-18 mr-2"></span>
                            <span>發布動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="" class="d-flex align-items-center active">
                            <span class="icon-menu-2 f-18 mr-2"></span>
                            <span>全體動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="" class="d-flex align-items-center">
                            <span class="icon-menu-3 f-18 mr-2"></span>
                            <span>好友動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="" class="d-flex align-items-center">
                            <span class="icon-menu-4 f-18 mr-2"></span>
                            <span>追蹤動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="" class="d-flex align-items-center">
                            <span class="icon-menu-5 f-18 mr-2"></span>
                            <span>熱門動態</span>
                        </a>
                    </li>
                    <li>
                        <a href="" class="d-flex align-items-center">
                            <span class="icon-menu-6 f-18 mr-2"></span>
                            <span>附近動態</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- <div class="sidebar">
            <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                <span class="icon-menu f-38"></span>
            </button>
            <div class="header-user d-flex align-items-end">
                <div class="icon rounded-circle"></div>
                <div class="username">@nanase0525</div>
            </div>
            <ul>
                <li><a href="#1">Sidebar 1</a></li>
                <li><a href="#2">Sidebar 2</a></li></li>
            </ul>
        </div> -->
        <!-- content -->
        <main class="brickCollection brick">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-fill border-0" id="tab-area">
                <li class="nav-item">
                    <a class="nav-link px-0" id="nav-tab1" href="brickGeneral.php">
                        <span class="icon-general color-dark f-28"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <div class="nav-link px-0 active" id="nav-tab2">
                        <span class="icon-collection2 color-dark f-28"></span>
                    </div>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="general tab-pane fade show active">
                    <div class="general-container container-fluid px-0">
                        <div class="general-row row">
                            <a class="general-column square col-4" href="#1">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/311/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#2">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/304/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#3">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/221/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#4">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/211/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#5">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/204/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#6">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/121/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#7">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/111/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#8">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/104/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#9">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/88/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#10">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/74/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#11">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/80/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#12">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/72/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#13">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/60/320/300)"></span>
                            </a>
                            <a class="general-column square col-4" href="#14">
                                <span class="general-box square-box background-image-center d-block w-100 h-100" style="background: url(https://picsum.photos/id/50/320/300)"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>