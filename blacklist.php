<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <a href="personInfo.php" class="col-2"><span class="icon-back"></span></a>
            <h3 class="col-8">黑名單</h3>
            <div class="col-2"></div>
        </header>
        <!-- content -->
        <main class="blackListGroup memberGroup">
            <div class="innerContent">
                <div class="group pt-0">
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/1.jpg)"></div>
                            <span>麻友</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/2.jpg)"></div>
                            <span>咪醬</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/3.jpg)"></div>
                            <span>美金大</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/4.jpg)"></div>
                            <span>小楓</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/5.jpg)"></div>
                            <span>桃子</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/6.jpg)"></div>
                            <span>米莎前輩</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/7.jpg)"></div>
                            <span>仁美</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/8.jpg)"></div>
                            <span>平手友梨奈</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                    <div class="info">
                        <a href="othersInfo.php" class="info">
                            <div class="avatar rounded-circle" style="background-image:url(styles/images/deleteMember/9.jpg)"></div>
                            <span>栗子</span>
                        </a>
                        <button type="button" class="rounded-pill f-14">取消封鎖</button>
                    </div>
                </div>
            </div>
        </main>

        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>