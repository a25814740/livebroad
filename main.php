<main class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active h-100" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
        <section class="banner-swiper swiper-container">
            <div class="swiper-wrapper">
                <!-- <div class="swiper-slide">
                    <div class="card text-white w-100 border-0 rounded-0">
                        <img src="https://picsum.photos/id/600/1200/550" class="card-img rounded-0" alt="...">
                        <div class="card-img-overlay d-flex align-items-center justify-content-center">
                            <div class="card-title m-0">
                                <h3 class="anchor-name" data-swiper-parallax="-100">活動砸大錢拿遊戲幣</h3>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="swiper-pagination"></div>
        </section>

        <section class="lists container-fluid p-1">
            <div class="row no-gutters w-100">
                <!-- 測試用格式 內容在 js -->
                <!-- <div class="col-6 lists-item">
                    <a href="#1" class="d-block h-100">
                        <span></span>
                        <div class="card text-white w-100 h-100 p-1 border-0">
                            <div class="card-img w-100 h-100" style="background: url('https://images.pexels.com/photos/1382731/pexels-photo-1382731.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') 50% 50% / cover no-repeat;"></div>
                            <div class="card-img-overlay">
                                <p class="hot">
                                    <span>131萬</span>
                                </p>
                                <div class="card-title">
                                    <p class="anchor-label" style="background: #f66767;">小時明星榜 No.1</p>
                                    <h3 class="anchor-name">可可</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </section>
    </div>

    <div class="tab-pane fade h-100" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
        <!-- <div class="d-flex justify-content-center align-items-center text-center h-100">
            <div class="edit">
                <h2>遊戲頁</h2>
                <p>還沒做啦 !!</p>
            </div>
        </div> -->
        <div id="games" class="games">
            <div class="games-lists lists container-fluid p-1">
                <div class="row no-gutters w-100">
                    <!-- <div class="games-lists-item lists-item col-6">
                        <a class="games-lists-item-link" href="#game1" data-game-num="22">
                            <div class="card text-white w-100 p-1 border-0">
                                <img src="http://h5play.com.tw/img/cache/original/game/2018/12/5c231938e2cfa4.66959185.png" alt="御劍雙修">
                            </div>
                        </a>
                    </div>
                    <div class="games-lists-item lists-item col-6">
                        <a class="games-lists-item-link" href="#game2" data-game-num="23">
                            <div class="card text-white w-100 p-1 border-0">
                                <img src="http://h5play.com.tw/img/cache/original/game/2019/01/5c4019b67c16d1.43267077.png" alt="神羅天征">
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane fade h-100" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
        <section class="lists container-fluid p-1">
            <div class="row no-gutters w-100">
                <!-- 測試用格式 內容在 js -->
                <!-- <div class="col-6 lists-item">
                    <a href="#1" class="d-block h-100">
                        <span></span>
                        <div class="card text-white w-100 h-100 p-1 border-0">
                            <div class="card-img w-100 h-100" style="background: url('https://images.pexels.com/photos/1382731/pexels-photo-1382731.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') 50% 50% / cover no-repeat;"></div>
                            <div class="card-img-overlay">
                                <p class="hot">
                                    <span>131萬</span>
                                </p>
                                <div class="card-title">
                                    <p class="anchor-label" style="background: #f66767;">小時明星榜 No.1</p>
                                    <h3 class="anchor-name">可可</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </section>
    </div>

    <div class="tab-pane fade h-100" id="pills-4" role="tabpanel" aria-labelledby="pills-4-tab">
        <section class="lists container-fluid p-1">
            <div class="row no-gutters w-100">
                <!-- 測試用格式 內容在 js -->
                <!-- <div class="col-6 lists-item">
                    <a href="#1" class="d-block h-100">
                        <span></span>
                        <div class="card text-white w-100 h-100 p-1 border-0">
                            <div class="card-img w-100 h-100" style="background: url('https://images.pexels.com/photos/1382731/pexels-photo-1382731.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') 50% 50% / cover no-repeat;"></div>
                            <div class="card-img-overlay">
                                <p class="hot">
                                    <span>131萬</span>
                                </p>
                                <div class="card-title">
                                    <p class="anchor-label" style="background: #f66767;">小時明星榜 No.1</p>
                                    <h3 class="anchor-name">可可</h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </section>
    </div>

</main>