<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
    <script src="https://use.fontawesome.com/aa95071b26.js"></script>
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header class="chatbox-header">
            <div class="chatHeader reviewMemberHeader d-flex justify-content-start align-items-center w-100 px-3">
                <div class="header-user d-flex align-items-center">
                    <a href="/" class="allow"><span class="icon-back"></span></a>
                    <div class="icon rounded-circle"></div>
                    <span class="title">桃子</span>
                </div>
                <div class="align-right ml-auto d-flex align-items-center">
                    <button type="button" class="no-bg-button">
                        <span class="icon-add-friend f-20 d-flex align-items-center"></span>
                    </button>
                    <button type="button" class="no-bg-button ml-2">
                        <span class="icon-blacklist f-20 d-flex align-items-center"></span>
                    </button>
                    <!-- d-none 隱藏 -->
                    <button type="button" class="no-bg-button ml-2 d-none">
                        <span class="icon-leave f-20 d-flex align-items-center"></span>
                    </button>
                </div>
            </div>
        </header>
        <!-- 側邊欄 -->
        <div class="sidebar">
            <button type="button" class="close-button icons sidebar-click-btn header-top-right">
                <span class="icon-menu f-38"></span>
            </button>
            <div class="header-user d-flex align-items-end">
                <div class="icon rounded-circle"></div>
                <div class="username">@nanase0525</div>
            </div>
            <ul>
                <li><a href="#1">Sidebar 1</a></li>
                <li><a href="#2">Sidebar 2</a></li></li>
            </ul>
        </div>
        <!-- content -->
        <main class="chatbox brick blur">
            <div class="chat-cont">
                <div class="chat-body">
                    <div class="chats-text-cont">
                        <p class="chat-text text-left">
                            <img class="rounded-circle img-fluid" src="https://picsum.photos/id/660/300/300" alt="">
                            <span class="text-buble">Hi</span>
                            <span class="time">13:43</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">Hello ~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-left">
                            <img class="rounded-circle img-fluid" src="https://picsum.photos/id/660/300/300" alt="">
                            <span class="text-buble"><img data-emoji-type="img" src="https://ppt.cc/f0LuSx" alt=""></span>
                            <span class="time">13:43</span>
                        </p>
                        <p class="chat-text text-left">
                            <img class="rounded-circle img-fluid" src="https://picsum.photos/id/660/300/300" alt="">
                            <button class="text-buble unlock layui-btn h-auto" id="layerUnlockBtn" type="button" data-method="unlock" data-type="auto">
                                <div class="picture blur">
                                    <img data-picture-type="img" src="https://picsum.photos/id/661/300/200" alt="">
                                </div>
                                <span class="text">花費<br>80鑽石<br>解鎖圖片</span>
                            </button>
                            <span class="time">13:43</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <!-- <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p>
                        <p class="chat-text text-right">
                            <span class="text-buble">~~~</span>
                            <span class="time">13:45</span>
                        </p> -->
                    </div>
                </div>
                <div class="chat-inp">
                    <div class="chat-inp-tools">
                        <div class="chat-inp-tools-btn"></div>
                        <div class="chat-inp-tools-wrapper">
                            <div class="chat-inp-tools-group">
                                <div class="chat-inp-tools-item chat-tool-presents"><img src="https://ppt.cc/fvW1ex" alt="" width="25"></div>
                                <div class="chat-inp-tools-item chat-tool-picture">
                                    <input type="file" name="" id="">
                                    <img src="https://ppt.cc/fiiubx" alt="" width="25">
                                </div>
                                <div class="chat-inp-tools-item chat-tool-emoji emoji"></div>
                            </div>
                        </div>
                    </div>
                    <div class="chat-input round-style">
                        <div class="input" contenteditable="true"></div>
                        
                        <div class="opts opts-inside">
                            <!-- <a class="send"></a> -->
                            <a class="send layui-btn bg-transparent p-0 d-flex align-items-center justify-content-center" id="layerUnlockBtn"  data-method="unlock" data-type="auto"></a>
                        </div>
                    </div>
                </div>
                <div class="emoji-dashboard tools-dashboard">
                    <ul class="emojis tools-dashboard-wrapper">
                        <li class="emoji tools-item" data-clipboard-text="img1"><img data-emoji-type="img" src="https://ppt.cc/f90bax" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img2"><img data-emoji-type="img" src="https://ppt.cc/f0LuSx" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img3"><img data-emoji-type="img" src="https://ppt.cc/fL22tx" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img4"><img data-emoji-type="img" src="https://ppt.cc/fhlZEx" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img5"><img data-emoji-type="img" src="https://ppt.cc/fJLP3x" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img6"><img data-emoji-type="img" src="https://ppt.cc/fSkiTx" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="img7"><img data-emoji-type="img" src="https://ppt.cc/fz7SHx" alt=""></li>
                        <li class="emoji tools-item" data-clipboard-text="gif1"><img data-emoji-type="img" src="https://ppt.cc/fXLVZx" alt=""></li>
                    </ul>
                </div>
                <div class="presents-dashboard giftGroup position-fixed">
                    <div id="main-wrapper" class="container-fluid p-0">
                        <!-- header -->
                        <header>
                            <a href="javascript:;" class="col-2"><span class="icon-back"></span></a>
                            <h3 class="col-8">贈禮選擇</h3>
                            <div class="col-2"></div>
                        </header>
                        <!-- content -->
                        <main class="giftGroup">
                            <div class="innerHeader d-flex justify-content-end align-items-center">
                                <span>目前鑽石:</span>
                                <span>723個</span>
                            </div>
                            <div class="group">
                                <div class="group-wrapper row no-gutters">
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-1.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">10</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-2.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">30</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-3.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">50</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-4.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">85</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-5.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">100</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-6.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">120</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-7.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">145</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-8.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">165</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-9.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">180</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-7.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">145</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-8.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">165</span>
                                        </div>
                                    </div>
                                    <div class="col-4 gift">
                                        <div class="giftImg" style="background-image:url(styles/images/gift/gift-9.svg)"></div>
                                        <div class="info">
                                            <div class="blue"></div>
                                            <span class="number">180</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="innerFooter">
                                <button type="button" class="giftBtn rounded-pill">確定送出</button>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </main>

    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>