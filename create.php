<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('include/include-head.php') ?>
</head>

<body>
    <div id="main-wrapper" class="container-fluid p-0">
        <!-- header -->
        <header>
            <h3>創建</h3>
        </header>
        <!-- content -->
        <main class="createGroup">
            <ul>
                <li>
                    <a href="createMap.php" class="d-flex justify-content-start align-items-center">
                        <div class="img" style="background-image:url(styles/images/common/position.svg)"></div>
                        <span>創建圖標<span class="icon-next f-14"></span></span>
                    </a>
                </li>
                <li>
                    <a href="createRecord.php" class="d-flex justify-content-start align-items-center">
                        <div class="img" style="background-image:url(styles/images/common/record.svg)"></div>
                        <span>創建紀錄<span class="icon-next f-14"></span></span>
                    </a>
                </li>
            </ul>
        </main>
        <?php include('footer.php') ?>
    </div>

    <?php include('include/include-js.php') ?>
</body>

</html>